
var express =require('express')
const jwt= require( 'jsonwebtoken')
const http = require('http');
const app =express();
var bodyparser=require("body-parser")
const mysql=require('mysql')

require('dotenv').config()



app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));

const connection=mysql.createConnection({
    host:process.env.host,
    user:process.env.user,
    password:process.env.password ,
    database:process.env.database   
})

console.log(process.env.database)
function toAuthtication(id){
    return jwt.sign({
        id:id,
    },process.env.securetkey)
  }
  
 
  app.post("/api/auth",(req,res)=>{
    console.log("welcomw")
       ;
        const {credentials}=req.body;    
        connection.query(`select * from users where userId=('${credentials.id}') and password =('${credentials.password}')`,
        function(errors,result,fields){
          
            if(result.length ===1) {
    
                res.json({user:{token:toAuthtication(credentials.id),types:result[0].types,username:result[0].userName}})
                console.log(result[0].userName)
    
            }else{
                res.status(400).json({result:{global:"Invaild credentail"}})
            }
           
        }
        )
    })
    app.get('/api/displayAllUser',(req,res)=>{
        console.log("welcometoalluser")
        
        const getAllusers=(`SELECT * FROM users WHERE types !='admin' ORDER BY id DESC;`)
        connection.query(getAllusers,(errors,results)=>{
            if(errors){
                res.status(400);
                res.end("error:" + errors);
               }
               res.json({displayUser:{displayUser:results}})
              // console.log(results)
            
        })
       
    })

    app.post("/api/editUser",(req,res)=>{
        console.log("edit")
       
        const {EditUserData}=req.body
        console.log(EditUserData)
       
          
      const EditUserQuery=`UPDATE users SET id='${EditUserData.idUserEdit}',
      userName='${EditUserData.empName}',userId='${EditUserData.empNumber}'
      ,password='${EditUserData.empPassword}',types='${EditUserData.department}' WHERE id='${EditUserData.idUserEdit}'`
    
      connection.query(EditUserQuery)
    
    
   
    })


/*------------------------admin get ----------------------*/


app.post("/api/getOrcasReport",(req,res)=>{


    const types_report=req.body.data

    const query=`SELECT * FROM evaluation_table WHERE type='${types_report.dep}' and year='${types_report.year}' ORDER BY id DESC`
    const query2=`SELECT DISTINCT year FROM evaluation_table  `; 

    connection.query(query,(errors,result)=>{

        if(errors){
            res.status(400);
            res.end("error:" + errors);
           }
           else{
            connection.query(query2,(err,resu)=>{

                res.json({orcasDailyReport:{orcasDailyReport:result,years:resu}})
            })
           }

        //   console.log(result)
        
    })
   
  
})

app.post("/api/getResData",(req,res)=>{

    var today=new Date();
    var month=(today.getMonth()+1)+'';

    if (month.length<2){
        month='0'+month    
    }
    var dayDate=today.getDate()+'';
    if(dayDate.length<2){
        dayDate='0'+dayDate;
    }
    var date=today.getFullYear()+'-'+ month+'-'+dayDate;

    
    if(req.body.data.test=='hello'){
        const query=`SELECT * FROM res_tabel where date_res>='${date}' ORDER BY id DESC`;
        connection.query(query,(errors,result)=>{

            if(errors){
                res.status(400);
                res.end("error:" + errors);
               }
               res.json({orcasResReport:{orcasResReport:result}})

        })
        
    
    }else
    {
        const query=`SELECT * FROM res_tabel where date_res<'${date}' and year='${req.body.data.year}' ORDER BY id DESC`;
        const query2=`SELECT DISTINCT year FROM res_tabel  `; 
        console.log(req.body.data)
        connection.query(query,(errors,result)=>{

            if(errors){
                res.status(400);
                res.end("error:" + errors);
               }
               else{
                   connection.query(query2,(err,resu)=>{
                    res.json({orcasResReport:{orcasResReport:result,years:resu}})
                   })
               }
 
        })
    }
    //const query=`SELECT * FROM res_tabel ORDER BY id DESC`;
  
})

app.post("/api/getQualitData",(req,res)=>{
 
    var today=new Date();
    var year =req.body.data
    console.log(req.body)
   
  

    const query=`SELECT * FROM quality_table WHERE quality_table.year='${year}'  ORDER BY id DESC`

    const query2=`SELECT DISTINCT year FROM quality_table  `; 
    
    connection.query(query,(errors,result)=>{

        if(errors){
            res.status(400);
            res.end("error:" + errors);
           }
           else{

            connection.query(query2,(err,resu)=>{
                if(err){
                    console.log(err)
                }

                res.json({orcasQualitReport:{orcasQualitReport:result , years:resu}})

            })
           }

        
    })
   
  
})

app.post(("/api/deleteDailyReport"),(req,res)=>{
   
    const {delete_id}=req.body


    const query=`DELETE evaluation_table,employee_status FROM evaluation_table  
    INNER JOIN employee_status on evaluation_table.id=employee_status.id_table 
    WHERE evaluation_table.id='${delete_id}'`

    connection.query(query,(errors,result)=>{
        
        if(errors){
            res.status(400);
            res.end("error:" + errors);
           }
           res.json({deleteDailyReport:{deleteDailyReport:"deleted"}})

    })

   
    
})

app.post("/api/deleteRevReport",(req,res)=>{
    
   
    const {id_deleteRes}=req.body
   // console.log("welcome to detelte rev")
    //console.log(req.body)

    const query_deleteRes=`DELETE FROM res_tabel WHERE id='${id_deleteRes}'`

    connection.query(query_deleteRes,(errors,result)=>{

        if(errors){
            res.status(400);
            res.end("error:" + errors);
           }
           res.json({deleteResReports:{deleteResReport:"deleted"}})


    })


})

app.post("/api/deleteQualityReport",(req,res)=>{
    
   
    const {id_deleteQuality}=req.body
   // console.log("welcome to detelte rev")
    //console.log(id_deleteQuality)
    const query_deleteRes=`DELETE FROM quality_table WHERE id='${id_deleteQuality}'`

    connection.query(query_deleteRes,(errors,result)=>{

        if(errors){
            res.status(400);
            res.end("error:" + errors);
           }
           res.json({deleteQualityReports:{deleteQualityReport:"deleted"}})

    })


})

// ---------------------------------------- insert new reservation ------------------------------------

app.post("/api/rev",(req,res)=>{
    const {data}=req.body;

    var today=new Date();
    var year =today.getFullYear();


    console.log(req.body)
    const INSERT_NOTE_QUERY=(`INSERT INTO res_tabel ( date, day, name,phone, resp_res, gar_res, price, res_normal, date_res , res_day , res_time , res_number , year) 
    VALUES ( '${data.date}', '${data.day}', '${data.name}', '${data.phone}', '${data.respons}', '${data.garsone}', '${data.price}', '${data.normalRes}', '${data.res_date}' ,'${data.res_day}' ,'${data.res_time}' ,'${data.res_number}' , '${year}');`);

    connection.query(INSERT_NOTE_QUERY,(errors,results)=>{
     if(!errors){ 

            res.json({revResult:{insertResult:true}});
        }
        });

       
})

// ------------------------------------ insert new daily report -----------------------

app.post("/api/eva",(req,res)=>{
    
    console.log("hello world")
    var today=new Date();
    var year =today.getFullYear();

    const {data} =req.body; 
    let x='';
    const testForm_query=(`SELECT * FROM evaluation_table where date=('${data.data.date}') and type=('${data.data.formType}') and shift=('${data.data.shift}') `)
    connection.query(testForm_query,(errors,result,feilds)=>{
        if(result.length == 0){
            const INSERT_NOTE_QUERY=(`INSERT INTO evaluation_table ( shift_resp, shift, date,day, emergency_state, notes,acheivement, type,year) 
            VALUES ( '${data.data.shiftResp}', '${data.data.shift}', '${data.data.date}', '${data.data.day}', '${data.data.emergencyState}', '${data.data.notes}', '${data.data.achievement}', '${data.data.formType}' ,'${year}');`);
            connection.query(INSERT_NOTE_QUERY,(errors,results)=>{
                if(!errors){ 
                    res.json({evaResult:{insertResult:true}});
                    x=results.insertId;
                    data.employeestate.map((employee,index)=>{
            
                        
                        const INSERT=(`INSERT INTO employee_status ( id_table, type_em, name,reason,time) 
                        VALUES ( '${x}', 'absence', '${employee.name}',  '${employee.reason}','');`);
                        connection.query(INSERT,(err,res)=>{

                        });
                    
                    })
            
                    data.employeeLate.map((employee,index)=>{
            
                    
                        const INSERT=(`INSERT INTO employee_status ( id_table, type_em, name,reason,time) 
                        VALUES ( '${x}', 'late', '${employee.name}',  '${employee.reason}' , '');`);
                        connection.query(INSERT,(err,res)=>{
                        });
                    
                    })
            
                    data.employeeLeft.map((employee,index)=>{
                        
                        const INSERT=(`INSERT INTO employee_status ( id_table, type_em, name,reason,time) 
                        VALUES ( '${x}', 'left', '${employee.name}',  '${employee.reason}' ,'${employee.time}');`);
                        connection.query(INSERT,(err,res)=>{

                        });
                 
                    })
            
                }
                else{
                    res.json({evaResult:{insertResult:false}});
                }
                
            //connection.end();
            })
        }
        else{
            res.json({evaResult:{insertResult:false}});

        }
    
    })

});

 // ------------------------- edit daily report ------------------------------

app.post('/api/eva_edit',(req,res)=>{
   
    //console.log("welcomw")
    console.log(req.body);
    var formType=req.body.data.formType;
    var shift=req.body.data.shift;
    var username=req.body.data.username;
    var id=req.body.data.id;

    console.log(id)
    var today=new Date();
    var date=today.getFullYear()+'-'+ (today.getMonth()+1)+'-'+today.getDate();
    const SELECT_QUERY=(`SELECT * FROM employee_status INNER JOIN evaluation_table on evaluation_table.id=employee_status.id_table where date=('${date}') and type=('${formType}') and shift=('${shift}')`);
    const SELECT_QUERY2=(`SELECT * FROM employee_status INNER JOIN evaluation_table on evaluation_table.id=employee_status.id_table where evaluation_table.id=('${id}')`);
    
    if(id!=null)
    {
        connection.query(SELECT_QUERY2,(error,main_table)=>{
            console.log('hello world')
            console.log(main_table)
            res.json({eva_editResult:{eva_editData:main_table}})     
    })  
    }
    else{
        connection.query(SELECT_QUERY,(error,main_table)=>{
                console.log('hello')
                res.json({eva_editResult:{eva_editData:main_table}})     
        })
    }

   
})


// ------------------------ delete and update row of employee status -------------


app.post('/api/delete_row',(req,res)=>{
   
    var id=(req.body.data)
    if(id != null){
        const DELETE_QUERY=`DELETE FROM employee_status WHERE id_em='${id}'`
        connection.query(DELETE_QUERY);

    }
})
app.post('/api/update_data',(req,res)=>{
   
    console.log(req.body.data.data.id);
    const data=req.body.data.data;
    const employeAbsence=req.body.data.employeeAbsence;
    const employeeLate=req.body.data.employeeLate;
    const employeeLeft=req.body.data.employeeLeft;

    if(employeAbsence.length >0) {
        employeAbsence.map((employee,id)=>{
            if(employee.id==null){
                const insertEmployee=(`INSERT INTO employee_status ( id_table, type_em, name,reason) 
                VALUES ( '${employee.id_table}', '${employee.type}', '${employee.name}',  '${employee.reason}');`);
                connection.query(insertEmployee)
            }
            if(employee.id!=null){
                const update_row=(`UPDATE employee_status SET name='${employee.name}' , reason='${employee.reason}' where id_em='${employee.id}'`);
                connection.query(update_row);
            }
        })
    }
    if(employeeLate.length >0 ){
        employeeLate.map((employee,id)=>{
            if(employee.id==null){
                const insertEmployee=(`INSERT INTO employee_status ( id_table, type_em, name,reason) 
                VALUES ( '${employee.id_table}', '${employee.type}', '${employee.name}',  '${employee.reason}');`);
                connection.query(insertEmployee)
            }
            if(employee.id!=null){
                const update_row=(`UPDATE employee_status SET name='${employee.name}' , reason='${employee.reason}' where id_em='${employee.id}'`);
                connection.query(update_row);
            }
        })
    }
    if(employeeLeft.length >0 ){
        employeeLeft.map((employee,id)=>{
            if(employee.id==null){
                const insertEmployee=(`INSERT INTO employee_status ( id_table, type_em, name,reason , time) 
                VALUES ( '${employee.id_table}', '${employee.type}', '${employee.name}',  '${employee.reason}' ,'${employee.time}');`);
                connection.query(insertEmployee)
            }
            if(employee.id!=null){
                const update_row=(`UPDATE employee_status SET name='${employee.name}' , reason='${employee.reason}',time='${employee.time}' where id_em='${employee.id}'`);
                connection.query(update_row);
            }
        })
    }



   const updatequery=`UPDATE evaluation_table SET shift='${data.shift}' ,emergency_state='${data.emergency_state}' ,notes='${data.notes}'
   ,acheivement='${data.acheivement}' where id='${data.id}' `
   connection.query(updatequery);


});


// --------------------- get all reservation from table depend on user type ----------

app.post('/api/get_allres',(req,res)=>{
    
    var today=new Date();
    var month=(today.getMonth()+1)+'';

    if (month.length<2){
        month='0'+month    
    }
    var dayDate=today.getDate()+'';
    if(dayDate.length<2){
        dayDate='0'+dayDate;
    }
    var date=today.getFullYear()+'-'+ month+'-'+dayDate;
   
    //console.log(req.body.data)'
    const SELECT_RES_QUERY=`SELECT * FROM res_tabel WHERE resp_res='${req.body.data}' and date_res>='${date}' ORDER BY id DESC`
   // const SELECT_RES_QUERY=`SELECT * FROM res_tabel WHERE resp_res='${req.body.data}' ORDER BY id DESC`
    connection.query(SELECT_RES_QUERY,(err,result)=>{
        if(!err){
       // console.log(result)
        res.json({allDataResult:{allData:result}})}
        else{
            console.log(err)
        }
        
    })
})

/// ------------------------------- delete and update reservation ------------------------------------

app.post('/api/delete_res',(req,res)=>{
   ;
    const DELETE_QUERY=`DELETE FROM res_tabel WHERE id='${req.body.data}'`
    connection.query(DELETE_QUERY);
    console.log(req.body.data)
});

app.post('/api/update_res',(req,res)=>{
   ;
    var data=req.body.data
    
   const updatequery=`UPDATE res_tabel SET name='${data.name}' ,phone='${data.phone}' ,resp_res='${data.respons}'
   ,gar_res='${data.garsone}', price='${data.price}', res_normal='${data.normalRes}',date_res='${data.res_date}' 
   ,res_time='${data.res_time}',res_number='${data.res_number}',res_day='${data.res_day}'
   where id='${data.id}' `
  
   connection.query(updatequery)
  // 
});


// ----------------------------- insert and get quality from db -----------------------------------------------

app.post('/api/insert_quality',(req,res)=>{
    var today=new Date();
    var year =today.getFullYear();

    var data=req.body.data;
    const SelectQuality=(`SELECT * FROM quality_table WHERE date=('${data.date}') and name=('${data.name}') `)
    connection.query(SelectQuality,(err,result)=>{
        if(result.length == 0){
            const insertQuality=(`INSERT INTO quality_table ( name, date, note ,year) 
            VALUES ( '${data.name}', '${data.date}', '${data.quality}' ,'${year}'); `);
            connection.query(insertQuality)
            res.json({allDataResult:{allData:true}})
        }
        else{
            res.json({allDataResult:{allData:false}})
        }
    })
})

app.post('/api/getInsert_quality',(req,res)=>{
   
    const data=req.body.data;

    const SELECT_QUERY=`SELECT * FROM quality_table WHERE date=('${data.date}') and name=('${data.name}')  ORDER BY id DESC`
    connection.query(SELECT_QUERY,(req,result)=>{
        console.log(result)
        if(result.length > 0){
            res.json({allDataResult:{allData:result}})
        }
       
    })

    console.log(req.body.data)

       
}) 

app.post('/api/updateQuality',(req,res)=>{
   ;
    console.log(req.body.data)
   const updatequery=`UPDATE quality_table SET note='${req.body.data.note}'where id='${req.body.data.id}' `
   connection.query(updatequery);


})

//----------------------------------------user modules------------------------------//
app.post("/api/createUser",(req,res)=>{
   ;
    const {data}=req.body
    console.log(data)   
    const INSERT_NOTE_QUERY=(`INSERT INTO users ( userName, userId, password,types) 
    VALUES ( '${data.empName}', '${data.empNumber}', '${data.empPassword}', '${data.department}');`);

    connection.query(INSERT_NOTE_QUERY,(errors,result)=>{

        if(errors){
             res.status(400);


             
             res.end("error:" + errors);
            }
            res.json({creactedUser:{CreatedUser:true}});

    })

   

})

app.post("/api/getUserId",(req,res)=>{
   
    const {idUser}=req.body
    console.log(idUser)
   
    const getInPutId=(`SELECT userId FROM users WHERE userId='${idUser}'`)
    connection.query(getInPutId,(errors,result)=>{
      
        if(result.length ===0) {
            res.json({userFound:{userFound:true}})
        }else{  
            res.json({userFound:{userFound:false}})
        }


    })
  // 


})

app.get('/api/displayAllUser',(req,res)=>{
    //console.log("welcometoalluser")
   
    const getAllusers=(`SELECT * FROM users WHERE types !='admin'  ORDER BY id DESC`)
    connection.query(getAllusers,(errors,results)=>{
        if(errors){
            res.status(400);
            res.end("error:" + errors);
           }
           res.json({displayUser:{displayUser:results}})
          // console.log(results)
        
    })
    //connection.end()
})

app.post("/api/deleteUser",(req,res)=>{
   
const {deleteUserId} =req.body;
console.log(deleteUserId)
 const deleteQuery=`DELETE FROM users WHERE id='${deleteUserId}'`;

    connection.query(deleteQuery,(errors,result)=>{
        if(errors){
            res.status(400);
            res.end("error:" + errors);
           }
           res.json({deleteUserResult:{deleteUserResult:"deleted"}})
         //  console.log(results)
    })

       
})  

app.post("/api/editUser",(req,res)=>{
   
    const {EditUserData}=req.body
    console.log(EditUserData)
   
      
  const EditUserQuery=`UPDATE users SET id='${EditUserData.idUserEdit}',
  userName='${EditUserData.empName}',userId='${EditUserData.empNumber}'
  ,password='${EditUserData.empPassword}',types='${EditUserData.department}'
   WHERE id='${EditUserData.idUserEdit}'`

  connection.query(EditUserQuery)


connection.end()
})



 app.use(express.static('build'))

 
app.get('/*',(req,res)=>{

    res.sendFile(`C:/Users/islam/Desktop/server-backend/build/index.html`);
    })

app.listen(4000,()=>console.log('running on localhost :4000'))


    