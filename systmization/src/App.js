import React, { Component } from 'react';
import './App.css';
import Login from './components/pages/login'
import Department from './components/pages/department'
import{connect} from "react-redux"
import UserRoute from "./route/userRoute"
import GustRoute from "./route/guestRoute"
import Zuwar from "./components/pages/zuwar"
import BIM from './components/pages/BIM'
import Orcas from './components/pages/Orcas'
import Future from './components/pages/Future'
import controlSystem from './components/pages/controlSystem'
import {Redirect} from 'react-router-dom'

class App extends Component {
 

  render() {

    return (
                
     
        
      <main >
        <GustRoute  location={this.props.location} exact path="/"  component={Login} ></GustRoute>

       <UserRoute location={this.props.location} path="/department" component={Department} ></UserRoute>

     {this.props.Types ==="admin" ?  <UserRoute location={this.props.location} path='/ControlSystemP' component={controlSystem}></UserRoute>:
      <Redirect to="/"></Redirect>
      }


        {this.props.Types ==="zuwar" ?  <UserRoute location={this.props.location} path='/Zuwar' component={Zuwar}></UserRoute>:
      <Redirect to="/department"></Redirect>
      }
      

         {this.props.Types ==="BIM"? 
        <UserRoute location={this.props.location} path='/BIM' component={BIM}></UserRoute>:
        <Redirect to="/department"></Redirect>
        
          }
       
           {this.props.Types==="orcas"?
            <UserRoute location={this.props.location} path='/orcas' component={Orcas}></UserRoute>:
            <Redirect to="/department"></Redirect>
            }
      

        {this.props.Types ==="future" ?
        < UserRoute location={this.props.location} path='/Future' component={Future}></UserRoute>:
        <Redirect to="/department"></Redirect>
        }
        
        {this.props.isAuthenticated ===false && <Redirect to="/"></Redirect>}

      </main>
   
            
    )
  }
}

     
 const mapToProps=(state)=>{
   return{
     Types:state.auth.types,
     isAuthenticated:!!state.auth.token,
   }
 }


export default connect(mapToProps)(App);
