import {REVINSERT,getUserId,createdUser,displayAllUser,deleteUserG,AllRevData,INSERT_QUALITY,All_QUALITYDATA,
  ORCAS_DAILY,ORCAS_Res,ORCAS_Quality,deleteDailyRep,deleteREsRep,deleteQualityRep
} from '../actions/types'


export default(state = {}, action)=>{
    switch (action.type) {
        case REVINSERT:
        return action.pass

        case getUserId:
        return action.userFound

        case createdUser:
        return action.userInsert

        case displayAllUser:
        return action.displayAlluserResult

        case deleteUserG:
          return  action.delteResultres
        
        case AllRevData:
        return action.allDataResult

        case INSERT_QUALITY:
        return action.allDataResult
        
        case All_QUALITYDATA:
        return action.allDataResult 
        case ORCAS_DAILY:
        return action.reuslt
        
        case ORCAS_Res:
        return action.reuslt

        case ORCAS_Quality:
        return action.reuslt

        case deleteDailyRep:
        return action.result

        case deleteREsRep:
        return action.result

      case deleteQualityRep:
       return action.result
        


      default:
        return state;
    }
  }
