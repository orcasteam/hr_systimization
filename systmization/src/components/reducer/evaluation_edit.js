import {EVA_EDIT} from '../actions/types'

export default(state = {}, action)=>{
    switch (action.type) {
        case EVA_EDIT:
        return action.evaluation_edit
        
      default:
        return state;
    }
  }
