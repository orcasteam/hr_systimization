import {USER_LOGGED_IN,USER_LOGGED_OUT,USER_TYPE,REVINSERT}from '../actions/types'

export default(state = {}, action)=>{
    switch (action.type) {

        case USER_LOGGED_IN:
         return action.user;
        
        case USER_LOGGED_OUT:
         return {}

      

      default:
        return state;
    }
  }
