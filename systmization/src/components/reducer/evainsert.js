import {EVAINSERT} from '../actions/types'

export default(state = {}, action)=>{
    switch (action.type) {
        case EVAINSERT:
        return action.evaluation
        
      default:
        return state;
    }
  }
