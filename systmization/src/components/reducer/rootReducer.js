import {combineReducers} from 'redux'
import user from './user'
import revinser from './revinsert'
import evainsert from './evainsert'
import evaluation_edit from './evaluation_edit'

export default combineReducers({
  auth:user,
  pass:revinser,
  evaluation:evainsert,
  evaluation_edit:evaluation_edit,
});