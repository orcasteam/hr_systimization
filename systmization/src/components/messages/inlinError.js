import React,{Component} from "react";


class InlineError extends Component{
  
    render(){
   
        return(
   <span style={{ color: "#ae5856" ,display:'inline-block'}} > {this.props.children}</span>
        )
    }
}

export default InlineError