import React, { Component } from 'react';
import './SideDrawer.css'
import {connect} from "react-redux"
import {NavLink} from 'react-router-dom'
import FormRes from "../forms/formRes"
import {logout} from '../actions/auth'


class SideDrawer extends Component {
  state={
    ReportType:"",
    report:false,
    reservation:false,
    user:false,
    dailyReport:false,
    quality:false,
    futureAppre:false,
    BimAppre:false,
    revApper:false,
    dailyApper:false,
    quiltyApper:false

  } 
  revApperClickHandler = () => {
    this.setState((prevState) => {
      return {revApper: !prevState.revApper};
    });
  };

  dailyApperClickHandler = () => {
    this.setState((prevState) => {
      return {dailyApper: !prevState.dailyApper};
    });
  };

  quiltyApperClickHandler = () => {
    this.setState((prevState) => {
      return {quiltyApper: !prevState.quiltyApper};
    });
  };


  BimAppretClickHandler = () => {
    this.setState((prevState) => {
      return {BimAppre: !prevState.BimAppre};
    });
  };

  NewReportClickHandler = () => {
    this.setState((prevState) => {
      return {report: !prevState.report};
    });
  };

  NewReservationClickHandler = () => {
    this.setState((prevState) => {
      return {reservation: !prevState.reservation};
    });
  };

  userClickHandler = () => {
    this.setState((prevState) => {
      return {user: !prevState.user};
    });
  };
  NewQualityClickHandler= () =>{
    this.setState((prevState) =>{
      return {quality: !prevState.quality}
    })
  }
  futurClickHandler = () => {
    this.setState((prevState) => {
      return {futureAppre: !prevState.futureAppre};
    });
  };



handerRequest=value=>{
this.setState({
  value:value
})
//alert(this.state.value)

this.props.requestType(value)
  }
    render() {
      return (
      
       <nav className={this.props.className}>
       {/* ------------------------slide menu for zuwar-------------------------*/}       
       {this.props.value ==='Zuwar' && 
       
          <div>
      
      <ul>      
        <li><button className='side-drawer-nav' onClick={this.NewReportClickHandler} ><strong> تقرير اليومي</strong> </button></li>
        {this.state.report ===true &&
        <div>
             <button value="zuwarEnter"  onClick={()=>this.handerRequest("zuwarEnter")}  className='side-drawer-nav_il'><strong>ادخال التقرير</strong> </button>   
             <button value="zuwarDisplay" onClick={()=>this.handerRequest("zuwarDisplay")} className='side-drawer-nav_il'><strong> تعديل التقرير</strong> </button>
             <button value="zuwarEdit"   className='side-drawer-nav_il' onClick={()=>this.handerRequest("zuwarEdit")}><strong> عرض التقرير</strong> </button>

      </div>  
        }
        
        <li><button className='side-drawer-nav' onClick={this.NewReservationClickHandler}><strong>حجوزات</strong> </button></li> 
        {this.state.reservation ===true &&
        <div>
             <button value="zuwarResEnter"  onClick={()=>this.handerRequest("zuwarResEnter")}  className='side-drawer-nav_il'><strong>حجز جديد </strong> </button>   
             <button value="zuwarResDisplay"  onClick={()=>this.handerRequest("zuwarResDisplay")} className='side-drawer-nav_il'><strong>  عرض الحجوزات</strong> </button>
           
      </div>  
        }

        <li><button className='side-drawer-nav' onClick={this.NewQualityClickHandler}><strong>تقرير الجودة</strong> </button></li>         
        {this.state.quality ===true && 
          <div>
            <button value="zuwarQualityEnter" onClick={()=>this.handerRequest("zuwarQualityEnter")} className='side-drawer-nav_il'><strong>تقرير جديد</strong> </button>
            <button value="zuwarQualityDisplay" onClick={()=>this.handerRequest("zuwarQualityDisplay")} className='side-drawer-nav_il'><strong> تعديل التقرير</strong> </button>
            <button value="zuwarQualityEdit" onClick={()=>this.handerRequest("zuwarQualityEdit")} className='side-drawer-nav_il'><strong> عرض التقرير</strong> </button>

          </div>
        }
     
      </ul>  

      </div>      
      }
    
    
    {/* ------------------------slide menu for BIM-------------------------*/}
    {this.props.value =='BIM' &&

    <div>

         
      <ul>      
        <li><button className='side-drawer-nav' onClick={this.NewReportClickHandler} ><strong> تقرير اليومي</strong> </button></li>
        {this.state.report ===true &&
        <div>
             <button value="BIMEnter"  onClick={()=>this.handerRequest("BIMEnter")}  className='side-drawer-nav_il'><strong>ادخال التقرير</strong> </button>   
             <button value="BIMDisplay"className='side-drawer-nav_il' onClick={()=>this.handerRequest("BIMDisplay")}><strong> تعديل التقرير</strong> </button>
             <button value="BIMEdit"   className='side-drawer-nav_il' onClick={()=>this.handerRequest("BIMEdit")}><strong> عرض التقرير</strong> </button>

      </div>  
        }
      </ul>
    </div>
    
    }       
      

     {/* ------------------------slide menu for Orcas-------------------------*/}
     {this.props.value =='orcas' &&

<div>

     
  <ul>      
    <li><button className='side-drawer-nav' onClick={this.NewReportClickHandler} ><strong> تقرير اليومي</strong> </button></li>
    {this.state.report ===true &&
    <div>
         <button value="orcasEnter"  onClick={()=>this.handerRequest("orcasEnter")}  className='side-drawer-nav_il'><strong>ادخال التقرير</strong> </button>   
         <button value="orcasDisplay"className='side-drawer-nav_il'  onClick={()=>this.handerRequest("orcasDisplay")} ><strong> تعديل التقرير</strong> </button>
         <button value="orcasEdit"   onClick={()=>this.handerRequest("orcasEdit")}  className='side-drawer-nav_il'><strong> عرض التقرير</strong> </button>

  </div>  
    }
  </ul>
</div>

} 


  {/* ------------------------slide menu for Future-------------------------*/}
  {this.props.value =='Future' &&

<div>

     
  <ul>      
    <li><button className='side-drawer-nav' onClick={this.NewReportClickHandler} ><strong> تقرير اليومي</strong> </button></li>
    {this.state.report ===true &&
    <div>
         <button value="FutureEnter"  onClick={()=>this.handerRequest("FutureEnter")}  className='side-drawer-nav_il'><strong>ادخال التقرير</strong> </button>   
         <button value="FutureDisplay"className='side-drawer-nav_il'onClick={()=>this.handerRequest("FutureDisplay")}><strong> تعديل التقرير</strong> </button>
         <button value="FutureEdit"   className='side-drawer-nav_il' onClick={()=>this.handerRequest("FutureEdit")}><strong> تعديل التقرير</strong> </button>

  </div>  
    }
  </ul>

 
</div>

} 


  {/* ------------------------slide menu for administrator-------------------------*/}    
                                    

 {this.props.value =='admin' &&

  <div>

     
    <ul>      

    <li><button className='side-drawer-nav' onClick={this.userClickHandler} ><strong>المستخدمين</strong> </button></li>

    {this.state.user ===true &&

    <div>

         <button value="FutureEnter"  onClick={()=>this.handerRequest("createNewUser")}  className='side-drawer-nav_il'><strong>انشاء مستخدم جديد</strong> </button>   
         <button value="FutureDisplay" onClick={()=>this.handerRequest("EditAndDelete")} className='side-drawer-nav_il'><strong> تعديل او حذف مستخدم</strong> </button>
        
  </div>  

    }
    <li><button className='side-drawer-nav' onClick={this.NewReservationClickHandler} ><strong >zuwar </strong> </button></li>

{this.state.reservation ===true &&
<div>
 <li> <button   onClick={this.dailyApperClickHandler}  className='side-drawer-nav_il'><strong> الحجوزات</strong> </button> </li>
  {this.state.dailyApper === true &&
  <div>
  <button   onClick={()=>this.handerRequest("ViewRevReportZuwer")}  className='side-drawer-goIN'><strong>عرض الحجوزات القادمة </strong> </button>
  <button   onClick={()=>this.handerRequest("ViewPreviousRevReportZuwer")}  className='side-drawer-goIN'><strong>عرض الحجوزات السابقة </strong> </button>
  <button   onClick={()=>this.handerRequest("EnterRevReportZuwer")}  className='side-drawer-goIN'><strong>اضافة تقارير</strong> </button>
  </div>
  }
  <button  onClick={this.quiltyApperClickHandler} className='side-drawer-nav_il'><strong> التقارير اليومي</strong> </button>
 {this.state.quiltyApper ==true && <div>
  <button   onClick={()=>this.handerRequest("ViewDailyReportZuwer")}  className='side-drawer-goIN'><strong> عرض التقارير اليومية</strong> </button>
  <button   onClick={()=>this.handerRequest("EnterViewDailyReportZuwer")}  className='side-drawer-goIN'><strong>اضافة تقارير</strong> </button>

 </div>}

  <button  onClick={this.revApperClickHandler} className='side-drawer-nav_il'><strong>تقارير الجودة</strong> </button>
 {this.state.revApper === true && <div>
  <button   onClick={()=>this.handerRequest("ViewQualityReportZuwer")}  className='side-drawer-goIN'><strong>  عرض التقارير الجودة </strong> </button>
  <button   onClick={()=>this.handerRequest("EnterViewQualityReportZuwer")}  className='side-drawer-goIN'><strong>اضافة تقارير</strong> </button>

 </div> }
 
  </div>
}

   <li><button className='side-drawer-nav' onClick={this.NewReportClickHandler} ><strong > Orcas</strong> </button></li>
   {this.state.report ===true && 
   <div>
   <button   onClick={()=>this.handerRequest("ViewOrcasReport")}  className='side-drawer-nav_il'><strong>  تقارير الشركة </strong> </button> 
   <button   onClick={()=>this.handerRequest("addOrcasReport")}  className='side-drawer-nav_il'><strong> اضافة تقرير يومي </strong> </button>     
   </div>
  }

   <li><button className='side-drawer-nav' onClick={this.BimAppretClickHandler} ><strong > BIM </strong> </button></li>
   {this.state.BimAppre ==true && <div>
    <button   onClick={()=>this.handerRequest("ViewBIMReport")}  className='side-drawer-nav_il'><strong>  تقارير الشركة </strong> </button> 
    <button    onClick={()=>this.handerRequest("addBIMReport")}  className='side-drawer-nav_il'><strong> اضافة تقرير يومي </strong> </button>     
   </div>}
   <li><button className='side-drawer-nav' onClick={this.futurClickHandler} ><strong > Future   </strong> </button></li>
  {this.state.futureAppre===true && 
  <div>
  <button   onClick={()=>this.handerRequest("ViewFutureReport")}  className='side-drawer-nav_il'><strong>  تقارير الشركة </strong> </button> 
  <button   onClick={()=>this.handerRequest("AddFutureReport")}  className='side-drawer-nav_il'><strong> اضافة تقرير يومي </strong> </button>    
  </div>
  }
  </ul>


 
</div>

} 




     
  



<ul>
<div>

  <button  className='side-drawer-nav'  onClick={()=>this.props.logout()} ><strong>تسجيل الخروج</strong> </button>   
    
    
</div>  
</ul>

        </nav>
        

        
 
      );
    }
  }
  
  
  
  const mapDispatch=dispatch=>{
    return{
      logout:()=>dispatch(logout())
    }
  }
  
  
export default connect(null,mapDispatch)(SideDrawer);