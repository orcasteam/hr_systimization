import React, { Component } from 'react';
import './Toolbar.css';
import {NavLink  } from 'react-router-dom'
import SideDrawer from '../SideDrawer/SideDrawer'

class Toolbar extends Component {

state={
show:false,
drawerClasses:"side-drawer open",
}


handlerShowside = () => {
  this.setState({
    show:!this.state.show,

  })

  if(this.state.show === false){
this.setState({drawerClasses:"side-drawer"})
  }else{
    this.setState({drawerClasses:"side-drawer open"})
  }
  
};

hanlderRequest=value=>{
  //alert(value)

 this.props.handlerType(value)
}
  render() {
    
      return (
      
        <div>
        <header className="toolbar">
      <nav className="toolbar__navigation">
      <div>

        <button className="toggle-button" onClick={this.handlerShowside}>
        <div className="toggle-button__line" />
        <div className="toggle-button__line" />
        <div className="toggle-button__line" />
             </button>
      
           </div>
            <NavLink to="/department" className="toolbar__logo">M&s</NavLink>
            <div className="spacer" />  
            
        </nav>
      </header>

     
 <SideDrawer className={this.state.drawerClasses} value={this.props.value} requestType={this.hanlderRequest}></SideDrawer>
    
      </div>
         
      
      );
    }
  }
export default Toolbar;