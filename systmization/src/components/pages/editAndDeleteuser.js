import React,{Component} from "react"
import {connect} from "react-redux"
import {editAllUser,deleteUser} from "../actions/auth"
//import { userInfo } from "os";
//import { throws } from "assert";
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'
//import ReactDom from 'react-dom'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import Modal from "./modal"

function searchingFor(term){
    
    return function(x){
        return x.userName.toLowerCase().includes(term.toLowerCase())|| !term;
        
    }
}

class EditAndDelete extends Component{

      constructor(props){
          super(props)
          this.state={
              displyAllUsers:'',
              term:'',
              show:false,
              userEditId:'',
              userEditData:{},
        
          }
      }

      showModal=(info)=>{
        // this.setState({show:true,userEditData:{...this.state.userEditData,info.value}})

        this.setState({
            userEditData:info,
            show:true
        },()=>{
           console.log(this.state.userEditData)
        })

       

      }
      hideModal=()=>{
          this.setState({show:false})
      }

    componentDidMount(){

        this.props.displayUser()

        setTimeout(function() { //Start the timer
            this.setState({
                displyAllUsers:this.props.showUser
            })//After 1 second, set render to true
        }.bind(this), 500)
    }

   handleSearchItem=(event)=>{ 
       this.setState({
           term:event.target.value
       })
   }

   handerDeleteItem=(itemDelete)=>{
      
    // alert("welcome"+itemDelete)
       const newDispalyUSer=this.state.displyAllUsers.filter(item=>{
           return item.id!=itemDelete
       })
    

       this.setState({
        displyAllUsers:[...newDispalyUSer]
       })

       this.props.deleteUser(itemDelete)

   }

   submit = (id) => {

    confirmAlert({
      title: 'تأكيد الحذف',
      //message: 'هل انت متأكد',
      buttons: [
        {
          label: 'نعم',
          
          onClick: () =>this.handerDeleteItem(id)
        },
        {
          label: 'لا',
          //onClick: () => alert('Click No')
        }
      ]
    })
  };

  handlerUpdateUser=(updated)=>{
   // console.log(updated)
    //console.log(this.state.displyAllUsers)
  {  updated && NotificationManager.success( 'تم تعديل المستخدم بنجاح' )}
   

  setTimeout(function() { //Start the timer
    this.setState({
        show:false
    })//After 1 second, set render to true
           

}.bind(this), 1000)
 
  
    
this.state.displyAllUsers.map((user,id)=>{
    
    if(user.id === updated.idUserEdit){
        user.id=updated.idUserEdit
        user.userName=updated.empName
        user.userId=updated.empNumber
        user.password=updated.empPassword
        user.types=updated.department
    }

}) 

console.log(this.state.displyAllUsers)
   }

  
    
    render(){

       //console.log(this.state.displyAllUsers)
//console.log("WELcome".toLowerCase())
     
        return(
        
        
           
         <div className='row'>
         
  
            <div className='main_div col-m-12 col-s-12 col-xs-12' >
   
            <div className='tabel_Edit_div col-m-9 col-s-12 col-s-12'>
         
       <form>
            <div className='title_div col-m-12 col-s-12 col-xs-12 '>
                    <input type="text"
                    id="myInput" 
                    onChange={this.handleSearchItem}
                    value={this.state.term}
                    placeholder="ابحث من خلال الاسم "
                    title="Type in a name"/>
                </div> 
                </form>         
                
            <div className='row'>
            
   
               <div className='form_sub_div col-m-12 col-s-12 col-xs-12'> 
              
                   <table id="myTable">
                   <tr class="header">
                                    <th >الاسم</th>
                                    <th >رقم المستخدم</th>
                                    <th >الصنف</th>
                                    <th> تعديل</th>
                                    <th> حذف</th>
                                </tr>
                                <Modal show={this.state.show}
                                 handleClose={this.hideModal} 
                                 userEditType={this.state.userEditData} 
                                updatedUserResult={this.handlerUpdateUser} >
                                   </Modal>
                       
     {this.state.displyAllUsers&& this.state.displyAllUsers.filter(searchingFor(this.state.term)).map((info,idx)=>{
                           
                               return(
                                  
                               
                               <tr>
                                   
                               <td>{info.userName}</td>
                               <td>{info.userId}</td>
                               <td>{info.types}</td>
                               <td>
            
                                   <button 
                               onClick={()=>this.showModal(info)}
                               className="button_edit" >نعديل</button></td>
                               <td><button 
                               className="button_delete"
                               onClick={()=>this.submit(info.id)}>حذف</button></td>

                                     </tr>
                                     
                               )

                                  })} 
              

                    </table>
                 
             
               </div> 
                 
           </div>    
            
         
   
   
         
            
                  
            </div>
           
           </div>
   
           </div>
           
        )
    }
}
const mapToprops=(state)=>{
    return{
  showUser:state.pass.displayUser
    }
}
const mapDispatch=dispatch=>{
    return{

        displayUser:()=>dispatch(editAllUser()),
        deleteUser:(deleteUserId)=>dispatch(deleteUser(deleteUserId))
    }
}
//const container =document.createElement("div")
//document.body.appendChild(container)
//ReactDom.render(<EditAndDelete/>,container)

export default connect(mapToprops,mapDispatch)(EditAndDelete)

