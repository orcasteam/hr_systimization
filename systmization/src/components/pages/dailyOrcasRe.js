import React,{Component} from "react"
import {connect} from "react-redux"
//import { push } from "react-router-redux";
import {editAllUser,deleteUser,deleteDailyReport} from "../actions/auth"
//import { userInfo } from "os";
//import { throws } from "assert";
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import {get_dailyReportOrcas,get_ResReportOrcas} from '../actions/auth' 
import FormEvaAdminDisplay from '../forms/formEvaAdminDisplay'
//import {Pagination} from 'react-bootstrap'
import Modal from "./modal"


function searchingFor(term){
    
    return function(x){
        return x.date.toLowerCase().includes(term.toLowerCase())|| !term;
        
    }
}

const today=new Date();
class dailyOrcasReport extends Component{

      constructor(props){
          super(props)
          this.state={
              displyAllUsers:'',
              term:'',
              show:false,
              userEditId:'',
              userEditData:{},
              activePage: 15,
              displayUser:false,
              info:'',
              editUser:false,
              year:today.getFullYear(),
              years:'',
              dep:'',
        
          }
      }

      showModal=(info)=>{
        // this.setState({show:true,userEditData:{...this.state.userEditData,info.value}})

        this.setState({
            userEditData:info,
            show:true
        },()=>{
           console.log(this.state.userEditData)
        })

       

      }
      hideModal=()=>{
          this.setState({show:false})
      }

    componentDidMount(){

      //  console.log(this.props.reportTypes)

        this.setState({
            displyAllUsers:''
        })

        if(this.props.reportTypes==="ViewOrcasReport"){
            this.props.getReportOrcas({dep:"orcasEnter",year:this.state.year})
        }else if(this.props.reportTypes==="ViewBIMReport"){
            this.props.getReportOrcas({dep:"BIMEnter",year:this.state.year})
        }else if (this.props.reportTypes==="ViewFutureReport"){
            this.props.getReportOrcas({dep:"FutureEnter",year:this.state.year})
        }else if  (this.props.reportTypes==="ViewDailyReportZuwer"){
            this.props.getReportOrcas({dep:"zuwarEnter",year:this.state.year})
        }else if  (this.props.reportTypes==="ViewRevReportZuwer"){
            this.props.getReportResOrcas()
        }


        setTimeout(function() { //Start the timer
            this.setState({
                displyAllUsers:this.props.showOrcasReports
            })//After 1 second, set render to true
            this.setState({
                years:this.props.years
            })
        }.bind(this), 500)
    

    }

   handleSearchItem=(event)=>{ 
       this.setState({
           term:event.target.value
       })
   }

   handerDeleteItem=(itemDelete)=>{
      
    // alert("welcome"+itemDelete)
       const newDispalyUSer=this.state.displyAllUsers.filter(item=>{
           return item.id!=itemDelete
       })
    

       this.setState({
        displyAllUsers:[...newDispalyUSer]
       })

       this.props.deleteDailyReport(itemDelete)

   }

   submit = (id) => {

    confirmAlert({
      title: 'تأكيد الحذف',
      //message: 'هل انت متأكد',
      buttons: [
        {
          label: 'نعم',
          
          onClick: () =>this.handerDeleteItem(id)
        },
        {
          label: 'لا',
          //onClick: () => alert('Click No')
        }
      ]
    })
  };

  display =(data)=>{
    this.setState({info:data})
    this.setState({displayUser:true})
        
  }
  deactive=()=>{
    this.setState({displayUser:false}) 
  }
  edit=(data)=>{
    this.setState({info:data})
    this.setState({editUser:true})
}
deactiveEdit=()=>{
    this.setState({editUser:false}) 
  }

  ChangeYear=(e)=>{
    this.setState({year:e.target.value}
    ,()=>{
        if(this.props.reportTypes==="ViewOrcasReport"){
            this.props.getReportOrcas({dep:"orcasEnter",year:this.state.year})
        }else if(this.props.reportTypes==="ViewBIMReport"){
            this.props.getReportOrcas({dep:"BIMEnter",year:this.state.year})
        }else if (this.props.reportTypes==="ViewFutureReport"){
            this.props.getReportOrcas({dep:"FutureEnter",year:this.state.year})
        }else if  (this.props.reportTypes==="ViewDailyReportZuwer"){
            this.props.getReportOrcas({dep:"zuwarEnter",year:this.state.year})
        }else if  (this.props.reportTypes==="ViewRevReportZuwer"){
            this.props.getReportResOrcas()
        }
        //this.props.getReportQualityOrcas(this.state.year)

        setTimeout(function() { //Start the timer
            this.setState({
                displyAllUsers:this.props.showOrcasReports
            })//After 1 second, set render to true

           // this.setState({years:this.props.years})
        }.bind(this), 500)
    })

}


    render(){

console.log(this.state.displyAllUsers)

let start_count =0;

     
        return(
        
        
           
         <div className='row'>
         
  
            <div className='main_div col-m-12 col-s-12 col-xs-12' >
   
            <div className='tabel_Edit_div col-m-9 col-s-12 col-s-12'>
         
       <form>
            <div className='title_div col-m-12 col-s-12 col-xs-12 '>
                    <input type="text"
                    id="myInput" 
                    onChange={this.handleSearchItem}
                    value={this.state.term}
                    placeholder="ابحث من خلال التاريخ (اليوم - التاريخ - السنة ) "
                    title="Type in a name"/>
                </div> 

                <div className='title_div col-m-12 col-s-12 col-xs-12 '>
                <select 
                
                onChange={this.ChangeYear}
                value={this.state.year}
                >
                    {this.state.years && this.state.years.map((item,id)=>{
                        return(
                            <option value={item.year}>{item.year}</option>
                        )
                    })}

                </select>
                </div>
                </form>         
                
            <div className='row'>
            
   
               <div className='form_sub_div col-m-12 col-s-12 col-xs-12'> 
              
                   <table id="myTable">
                   <tr class="header">
                
                                    <th >الاسم</th>
                                    <th >الفترة</th>
                                    <th >اليوم</th>
                                    <th >تاريخ</th>
                                    <th> عرض</th>
                                    <th> تعديل</th>
                                    <th> حذف</th>

                                </tr>
                                <Modal show={this.state.show}
                                 handleClose={this.hideModal} 
                                 userEditType={this.state.userEditData} 
                                updatedUserResult={this.handlerUpdateUser} >
                                   </Modal>
                       
                       {this.state.displayUser === true && <FormEvaAdminDisplay isDisplay={this.state.displayUser} data={this.state.info} deactive={this.deactive}></FormEvaAdminDisplay>}
                        {this.state.editUser === true && <FormEvaAdminDisplay isEdit={this.state.editUser} data={this.state.info} deactive={this.deactiveEdit}></FormEvaAdminDisplay>}
     {this.state.displyAllUsers&& this.state.displyAllUsers.filter(searchingFor(this.state.term)).map((info,idx)=>{
                           
                          // if(idx>=start_offest && start_count<per_page){
                              // start_count++;
                           
                               return(
                                  
                               
                               <tr>
                                   
                               <td>{info.shift_resp}</td>
                               <td>{info.shift}</td>
                               <td>{info.day}</td>
                               <td>{info.date}</td>
                               <td><button 
                               className="button_display"
                               onClick={()=>this.display(info)}>عرض</button></td>
                               <td><button 
                               className="button_edit"
                               onClick={()=>this.edit(info)}>تعديل</button></td>
                               <td><button 
                               className="button_delete"
                               onClick={()=>this.submit(info.id)}>حذف</button></td>

                                     </tr>
                                     
                               )
                            //}

                                  })} 
              

                    </table>
              

                        <div>
     
      </div>

                 
             
               </div> 
                 
           </div>    
            
         
   
   
         
            
                  
            </div>
           
           </div>
   
           </div>
           
        )
    }
}
const mapToprops=(state)=>{
    return{
  showOrcasReports:state.pass.orcasDailyReport ,
  years:state.pass.years,
  showOrcasRevReport:state.pass.orcasResReport,
//  page: Number(state.routing.locationBeforeTransitions.query.page) || 1,
    }
}
const mapDispatch=dispatch=>{
    return{

        displayUser:()=>dispatch(editAllUser()),
        deleteUser:(deleteUserId)=>dispatch(deleteUser(deleteUserId)),
        getReportOrcas:(data)=>dispatch(get_dailyReportOrcas(data)),        
        getReportResOrcas:()=>dispatch(get_ResReportOrcas()),
        deleteDailyReport:(id)=>dispatch(deleteDailyReport(id))
    }
}
//const container =document.createElement("div")
//document.body.appendChild(container)
//ReactDom.render(<EditAndDelete/>,container)

export default connect(mapToprops,mapDispatch)(dailyOrcasReport)

