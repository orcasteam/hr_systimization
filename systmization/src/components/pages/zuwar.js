import React,{Component} from 'react'
import Toolbar from '../Toolbar/Toolbar'
import FormRes from "../forms/formRes"
import FormEva from '../forms/FormEva';
import ChooseShift from '../forms/chooseShift'
import FormRes_edit from '../forms/formRes_edit'
import {insertRev, insertEva,insertEva_Edit} from "../actions/auth";
import {connect} from 'react-redux';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import QualityForm from '../forms/QualityForm'
import FormEvaDisplay from '../forms/formEvaDisplay'
import QualityDisplay from '../forms/QualityDisplay'

class Zuwar extends Component{
    
state={
    value:this.props.location.pathname.substring(1,6),
    type:"",
    errors:{},
    data:[],
    qualityUpdate:true,
    isDisplay:true,
    
}
 handlerRequest=(value)=>{
     this.setState({
         type:value
     })
  
 }

handerSubmit=(data)=>{     
this.props.revInsert(data)
setTimeout(function() { 
    window.location.reload(); 
}.bind(this), 1000)
 }

handlerEvaSubmit=(data)=>{
    this.props.evaInsert(data);
   // this.setState({data},()=>{console.log(this.state.data)})
 }

 reloadPage=(value)=>{
  
    setTimeout(function() { 
        window.location.reload(); 
    }.bind(this), 1000) 
}


    render(){ 
        return(
            
            <div>                  
                <p1>{this.state.errors.global}</p1>
             <Toolbar value={this.state.value} handlerType={this.handlerRequest} ></Toolbar>  
                {this.state.type ==="zuwarEnter"  &&  <FormEva FormType={this.state.type} submit={this.handlerEvaSubmit}></FormEva>}
                {this.state.type ==="zuwarDisplay"  &&   <ChooseShift FormType={this.state.type}></ChooseShift>}
                {this.state.type === "zuwarEdit" && <ChooseShift FormType={this.state.type} isDisplay={this.state.isDisplay}></ChooseShift>}

              
                {this.state.type ==="zuwarResEnter"  && <FormRes FormType={this.state.type} submit={this.handerSubmit}></FormRes>}
               {this.state.type==="zuwarResDisplay" && <FormRes_edit FormType={this.state.type}></FormRes_edit>}

                {this.state.type==="zuwarQualityEnter" && <QualityForm FormType={this.state.type} isNew={this.state.qualityUpdate}></QualityForm>}
                {this.state.type === "zuwarQualityDisplay"&& <QualityForm FormType={this.state.type} isUpdate={this.state.qualityUpdate}></QualityForm>}
                {this.state.type === "zuwarQualityEdit" && <QualityDisplay FormType={this.state.type}></QualityDisplay>}
               

            {this.props.ifInserted && this.reloadPage(this.props.ifInserted)}
            {this.props.ifInserted===false && this.reloadPage(this.props.ifInserted)}

            {this.props.ifInserted=== true &&  NotificationManager.success( '    تم اضافة تقييم جديد',' تقييم اليوم' )}
            {this.props.ifInserted=== false &&  NotificationManager.error( ' هناك خطأ' )}
                       <NotificationContainer/> 
             
             
          
          
           </div>
        );
    }
}

const mapdispatch = dispatch =>{
    return {
        revInsert:(data)=>dispatch(insertRev(data)),
        evaInsert:(data)=>dispatch(insertEva(data)),
        eva_editInsert:(data)=>dispatch(insertEva_Edit(data)),
        
    }
}
const maptToProps=(state)=>{
    return{
        isInserted:state.pass.insertResult,
        ifInserted:state.evaluation.insertResult,
        data:state.evaluation_edit.eva_editData,
       
    }
  }

export default connect(maptToProps,mapdispatch)(Zuwar)
