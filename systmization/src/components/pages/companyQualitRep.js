import React,{Component} from "react"
import {connect} from "react-redux"
import {editAllUser,deleteUser} from "../actions/auth"
//import { userInfo } from "os";
//import { throws } from "assert";
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import {get_dailyReportOrcas,get_ResReportOrcas,get_QualityReportOrcas,deleteQualityReport} from '../actions/auth' 
import Modal from "./modal"
import FormQualityDisplayAdmin from '../forms/formQualityDisplayAdmin'

function searchingFor(term){
    
    return function(x){
        return x.date.toLowerCase().includes(term.toLowerCase())|| !term;
        
    }
}
const today=new Date();

class companyQualityReport extends Component{

      constructor(props){
          super(props)
          this.state={
              displyAllUsers:'',
              term:'',
              show:false,
              userEditId:'',
              userEditData:{},
              editScreen:false,
              info:'',
              displayscreen:false,
              years:'',
              year:today.getFullYear(),
        
          }
      }

      showModal=(info)=>{
        // this.setState({show:true,userEditData:{...this.state.userEditData,info.value}})

        this.setState({
            userEditData:info,
            show:true
        },()=>{
           console.log(this.state.userEditData)
        })

       

      }
      hideModal=()=>{
          this.setState({show:false})
      }

    componentDidMount(){

     //   console.log(this.props.reportTypes)

       
       if(this.props.reportTypes==="ViewQualityReportZuwer"){
            this.props.getReportQualityOrcas(this.state.year)
        }


        setTimeout(function() { //Start the timer
            this.setState({
                displyAllUsers:this.props.showOrcasQualityReport
            })//After 1 second, set render to true

            this.setState({years:this.props.years})
        }.bind(this), 500)
    

    }

   handleSearchItem=(event)=>{ 
       this.setState({
           term:event.target.value
       })
   }

   handerDeleteItem=(itemDelete)=>{
      
    // alert("welcome"+itemDelete)
       const newDispalyUSer=this.state.displyAllUsers.filter(item=>{
           return item.id!=itemDelete
       })
    

       this.setState({
        displyAllUsers:[...newDispalyUSer]
       })

       this.props.deleteQualityRep(itemDelete)

   }

   submit = (id) => {

    confirmAlert({
      title: 'تأكيد الحذف',
      //message: 'هل انت متأكد',
      buttons: [
        {
          label: 'نعم',
          
          onClick: () =>this.handerDeleteItem(id)
        },
        {
          label: 'لا',
          //onClick: () => alert('Click No')
        }
      ]
    })
  };




   edit=(info)=>{
    this.setState({editScreen:true})
    this.setState({info:info})
  }

  deactive=(data)=>{
    console.log(data)        
    this.setState({editScreen:false}) 
    { data && NotificationManager.success( 'تم تعديل الحجز بنجاح' )}
    
    if(data!=null){
        this.state.displyAllUsers.map((item,id)=>{
           if(item.id === data.obj.id){
             item.note = data.obj.note   
            }
        })
    }
    
          
        
    
}

display=(Info)=>{
    this.setState({info:Info})
    this.setState({displayscreen:true})
}
deactiveDisplay=(data)=>{

    this.setState({displayscreen:false})
}

ChangeYear=(e)=>{
    this.setState({year:e.target.value}
    ,()=>{
        
        this.props.getReportQualityOrcas(this.state.year)

        setTimeout(function() { //Start the timer
            this.setState({
                displyAllUsers:this.props.showOrcasQualityReport
            })//After 1 second, set render to true

           // this.setState({years:this.props.years})
        }.bind(this), 500)
    })

}
    
    render(){

       //console.log(this.state.displyAllUsers)
//console.log("WELcome".toLowerCase())
console.log(this.state.displyAllUsers)
     
        return(
        
           
         <div className='row'>
         
  
            <div className='main_div col-m-12 col-s-12 col-xs-12' >
   
            <div className='tabel_Edit_div col-m-9 col-s-12 col-s-12'>
         
       <form>
        
            <div className='title_div col-m-12 col-s-12 col-xs-12 '>
                    <input type="text"
                    id="myInput" 
                    onChange={this.handleSearchItem}
                    value={this.state.term}
                    placeholder="ابحث من خلال التاريخ (اليوم - التاريخ - السنة ) "
                    title="Type in a name"/>
                
                </div> 
                <div className='title_div col-m-12 col-s-12 col-xs-12 '>
                <select 
                
                onChange={this.ChangeYear}
                value={this.state.year}
                >
                    {this.state.years && this.state.years.map((item,id)=>{
                        return(
                            <option value={item.year}>{item.year}</option>
                        )
                    })}

                </select>
                </div>
                </form>         
   
            <div className='row'>
            
   
               <div className='form_sub_div col-m-12 col-s-12 col-xs-12'> 
              
                   <table id="myTable">
                   <tr class="header">
                
                                    <th >الاسم</th>
                                    <th >تاريخ</th>
                                    <th >ملاحظات</th>
                                    <th> عرض</th>
                                    <th> تعديل</th>
                                    <th> حذف</th>

                                </tr>
                                <Modal show={this.state.show}
                                 handleClose={this.hideModal} 
                                 userEditType={this.state.userEditData} 
                                updatedUserResult={this.handlerUpdateUser} >
                                   </Modal>
                       
                       {this.state.displayscreen && <FormQualityDisplayAdmin data={this.state.info} deactive={this.deactiveDisplay}></FormQualityDisplayAdmin>}
                        {this.state.editScreen && <FormQualityDisplayAdmin data={this.state.info} deactive={this.deactive} isedit={this.state.editScreen}></FormQualityDisplayAdmin>}
     {this.state.displyAllUsers&& this.state.displyAllUsers.filter(searchingFor(this.state.term)).map((info,idx)=>{
                           
                               return(
                                  
                               
                               <tr>
                                   
                               <td>{info.name}</td>
                               <td>{info.date}</td>
                               <td>{info.note}</td>
                               <td><button 
                               className="button_display"
                               onClick={()=>this.display(info)}>عرض</button></td>
                               <td><button 
                               className="button_edit"
                               onClick={()=>this.edit(info)}>تعديل</button></td>
                               <td><button 
                               className="button_delete"
                               onClick={()=>this.submit(info.id)}>حذف</button></td>
                            
                            </tr>
                                     
                               )

                                  })} 
              

                    </table>
                 
             
               </div> 
                 
           </div>    
            
           
                  
            </div>
           
           </div>
   
           </div>
           
        )
    }
}
const mapToprops=(state)=>{
    return{
  showOrcasReports:state.pass.orcasDailyReport ,
  showOrcasRevReport:state.pass.orcasResReport,
  showOrcasQualityReport:state.pass.orcasQualitReport,
  years:state.pass.years,
    }
}
const mapDispatch=dispatch=>{
    return{
        displayUser:()=>dispatch(editAllUser()),
        deleteUser:(deleteUserId)=>dispatch(deleteUser(deleteUserId)),
        getReportOrcas:(data)=>dispatch(get_dailyReportOrcas(data)),
        getReportResOrcas:()=>dispatch(get_ResReportOrcas()),
        getReportQualityOrcas:(data)=>dispatch(get_QualityReportOrcas(data)),
        deleteQualityRep:(id)=>dispatch(deleteQualityReport(id))
    }
}
//const container =document.createElement("div")
//document.body.appendChild(container)
//ReactDom.render(<EditAndDelete/>,container)

export default connect(mapToprops,mapDispatch)(companyQualityReport)

