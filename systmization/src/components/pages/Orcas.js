import React,{Component} from 'react'
import FormEva from '../forms/FormEva';
import Toolbar from '../Toolbar/Toolbar'
import {insertEva,insertEva_Edit} from "../actions/auth";
import {connect} from 'react-redux';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import ChooseShift from '../forms/chooseShift'


class Orcas extends Component{

state={
    value:this.props.location.pathname.substring(1,7),
    type:"",
    isDisplay:true,
    
}
 handlerRequest=(value)=>{
     this.setState({
         type:value
     })
   
 }

 handlerEvaSubmit=(data)=>{
    this.props.evaInsert(data);
}

reloadPage=(value)=>{
    if(value){
    setTimeout(function() { 
        window.location.reload(); 
    }.bind(this), 1000)
    }
}
  
    render(){

        return(
        
            <div> 
            <Toolbar value={this.state.value} handlerType={this.handlerRequest} ></Toolbar>  
               {this.state.type ==="orcasEnter" && <FormEva FormType={this.state.type} submit={this.handlerEvaSubmit}></FormEva>  }
                {this.state.type ==="orcasDisplay" && <ChooseShift  FormType={this.state.type}></ChooseShift>}                
                {this.state.type ==="orcasEdit" && <ChooseShift  FormType={this.state.type} isDisplay={this.state.isDisplay}></ChooseShift>}
                
                {this.props.isInserted  && this.reloadPage(this.props.isInserted)}
                {this.props.isInserted=== true &&  NotificationManager.success( '    تم اضافة تقييم جديد',' تقييم اليوم' )}
                {this.props.isInserted=== false &&  NotificationManager.error( ' هناك خطأ' )}
           <NotificationContainer/>  

               
          </div>
        
        );
    }
}


const mapdispatch = dispatch =>{
    return {
        evaInsert:(data)=>dispatch(insertEva(data)),

        eva_edit:(data)=>dispatch(insertEva_Edit(data)),

    }
}

const maptToProps=(state)=>{
    return{
        isInserted:state.evaluation.insertResult
    }
  }

export default   connect(maptToProps,mapdispatch)(Orcas)
