import React, { Component } from 'react';
import {Route, Link ,NavLink ,Redirect } from 'react-router-dom';
import {connect} from 'react-redux'
import {logout} from '../actions/auth'
import Zuwar from "./zuwar"


class department extends Component {
 
  render() {
  
    return (
  
     
    <div className='component_page'>
    {/*this.props.isAuthenticated?<button onClick={()=>this.props.logout()}>logout</button>:<p1>again</p1>*/}
        <img className='div_imgM'  ></img>
        
        <div className='div_companies'>
        {this.props.Types ==="zuwar" &&   <NavLink to='/Zuwar' > <img className='div_imgZ'></img> </NavLink>}
        
          {this.props.Types ==="BIM" && <NavLink to='/BIM'> <img className='div_imgB'></img> </NavLink> }
          
          {this.props.Types ==="orcas" &&<NavLink to='/orcas'>  <img className='div_imgO'></img> </NavLink>  }
          
          {this.props.Types === "future" &&<NavLink to='/Future'>   <img className='div_imgF'></img> </NavLink>    }
          
          {this.props.Types === "admin" &&  <Redirect to="/ControlSystemP"></Redirect> }
      
            </div>

    </div>
 
   
  
    );
  }
}

const mapStateToprops=(state)=>{

  return{
   isAuthenticated:!!state.auth.token,
   Types:state.auth.types,
 
  }
}

const mapDispatch=dispatch=>{
  return{
    logout:()=>dispatch(logout())
  }
}

export default  connect(mapStateToprops,mapDispatch)(department);
