
import React,{Component} from 'react'
import {NavLink} from "react-router-dom"
import FormLogin from '../forms/formlogin'
import {login} from '../actions/auth'
import {connect} from 'react-redux'

class Login extends Component{

   
    state={
        errors:{}
    }
   
    submit=data=>{
      
   this.props.login(data).catch(err =>
    this.setState({ errors: err.response.data.result})).then(()=>
    {this.state.errors.global !== "Invaild credentail" && this.props.history.push("/Department")})

   


    }
    
    render(){   
        return(
            <div className="App">
            <div className="App__Aside">
        {this.state.errors.global}
            <div className="FormTitle">
                    <NavLink to="/sign-in" activeClassName="FormTitle__Link--Active" className="FormTitle__Link">Sign In</NavLink> 
                </div>
                
               <FormLogin submit={this.submit}></FormLogin>
              
            </div>
            
            <div className="App__Form">
              <div className="main-logo">    
              </div>
              <div className="down-logo">
              <div className="down-logo1"></div>
              <div className="down-logo2"></div>
              <div className="down-logo3"></div>
              <div className="down-logo4"></div>
          
              </div>
             
               
            </div>
      
          </div>
        );
       
    }
    
}


const mapDispatch=dispatch=>{

    return{
        login:(data)=>dispatch(login(data))
    }
}

export default connect(null,mapDispatch)(Login);