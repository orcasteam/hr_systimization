import React,{Component} from "react"
import {connect} from "react-redux"
import {editAllUser,deleteUser,get_dailyReportOrcas,get_ResReportOrcas,deleteResReport} from "../actions/auth"
//import { userInfo } from "os";
//import { throws } from "assert";
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'
import {NotificationContainer, NotificationManager} from 'react-notifications'; 
import Modal from "./modal"
import ResModal from '../forms/resModal'
import ResDisplay from '../forms/resDisplay'
import {Pagination} from "react-bootstrap"

function searchingFor(term){
    return function(x){
        return x.date_res.toLowerCase().includes(term.toLowerCase())|| !term;
        
    }
}
const today=new Date();
class PreviousRes extends Component{

      constructor(props){
          super(props)
          this.state={
              displyAllUsers:'',
              term:'',
              show:false,
              userEditId:'',
              userEditData:{},
              editScreen:false,
              info:'',
              displayscreen:false,
              activePage:1,
              itemPage:[],
              year:today.getFullYear(),
              years:'',
          }
      }

      showModal=(info)=>{
        // this.setState({show:true,userEditData:{...this.state.userEditData,info.value}})

        this.setState({
            userEditData:info,
            show:true
        },()=>{
           console.log(this.state.userEditData)
        })

       

      }
      hideModal=()=>{
          this.setState({show:false})
      }

    componentDidMount(){

     
       if(this.props.reportTypes==="ViewPreviousRevReportZuwer"){
            this.props.getReportResOrcas({test:"previos",year:this.state.year})
        }


        setTimeout(function() { //Start the timer
            this.setState({
                displyAllUsers:this.props.showOrcasRevReport
            }
            /*()=>{
                let items=[]
                const per_page=10;
                const pages=Math.ceil(this.state.displyAllUsers.length /per_page);
                for(let i=1; i<pages ; i++){
                    items.push(
                        <Pagination.Item onClick={()=>this.changePage(i)}>{i}</Pagination.Item>
                    )
                }
                this.setState({itemPage:items})
            })//After 1 second, set render to true

        */      
        )
        this.setState({years:this.props.years})
      console.log(this.state.years)
    }.bind(this), 500)
    

    }

    changePage=(page)=>{
        this.setState({activePage:page})
    }

   handleSearchItem=(event)=>{ 
    
       this.setState({
           term:event.target.value
       })
  
   }

   handerDeleteItem=(itemDelete)=>{
      
    // alert("welcome"+itemDelete)
       const newDispalyUSer=this.state.displyAllUsers.filter(item=>{
           return item.id!=itemDelete
       })
    

       this.setState({
        displyAllUsers:[...newDispalyUSer]
       })

       this.props.deleteQualityRep(itemDelete)

   }

   submit = (id) => {

    confirmAlert({
      title: 'تأكيد الحذف',
      //message: 'هل انت متأكد',
      buttons: [
        {
          label: 'نعم',
          
          onClick: () =>this.handerDeleteItem(id)
        },
        {
          label: 'لا',
          //onClick: () => alert('Click No')
        }
      ]
    })
  };


  edit=(info)=>{
    this.setState({editScreen:true})
    this.setState({info:info})
  }

  deactive=(data)=>{
        
    this.setState({editScreen:false})
    {  data && NotificationManager.success( 'تم تعديل الحجز بنجاح' )}

    if(data){
         this.state.displyAllUsers.map((item,id)=>{
           if(item.id == data.id){
                item.name=data.name
                item.date=data.date
                item.day=data.day
                item.date_res=data.res_date
                item.phone=data.phone
                item.price=data.price
                item.res_normal=data.normalRes
                item.gar_res=data.garsone
                item.resp_res=data.respons
                item.res_day=data.res_day
                item.res_number=data.res_number
                item.res_time=data.res_time 
                
            }
        })
          
        
    }
}

display=(Info)=>{
    this.setState({info:Info})
    this.setState({displayscreen:true})
}
deactiveDisplay=(data)=>{

    this.setState({displayscreen:false})
}

  handlerUpdateUser=(updated)=>{
   // console.log(updated)
    //console.log(this.state.displyAllUsers)
  {  updated && NotificationManager.success( 'تم تعديل المستخدم بنجاح' )}
   

  setTimeout(function() { //Start the timer
    this.setState({
        show:false
    })//After 1 second, set render to true
           

}.bind(this), 1000)
 
  
    
this.state.displyAllUsers.map((user,id)=>{
    
    if(user.id === updated.idUserEdit){
        user.id=updated.idUserEdit
        user.userName=updated.empName
        user.userId=updated.empNumber
        user.password=updated.empPassword
        user.types=updated.department
    }

}) 

console.log(this.state.displyAllUsers)
   }
   ChangeYear=(e)=>{


    this.setState({year:e.target.value}
        ,()=>{
            
            this.props.getReportResOrcas({test:"previos",year:this.state.year})
    
            setTimeout(function() { //Start the timer
                this.setState({
                    displyAllUsers:this.props.showOrcasRevReport
                })//After 1 second, set render to true
    
               // this.setState({years:this.props.years})
            }.bind(this), 500)
        })
   }
  
    
    render(){

       //console.log(this.state.displyAllUsers)
//console.log("WELcome".toLowerCase())

     const per_page=10;
     const start_offset=(this.state.activePage - 1)*per_page;
     let start_count=0;   
     return(
                   
         <div className='row'>
         
  
            <div className='main_div col-m-12 col-s-12 col-xs-12' >
   
            <div className='tabel_Edit_div col-m-9 col-s-12 col-s-12'>
         
       <form>
            <div className='title_div col-m-12 col-s-12 col-xs-12 '>
                    <input type="text"
                    className='col-m-4 col-s-4 col-xs-4'
                    id="myInput" 
                    onInput={this.handleSearchItem}
                    value={this.state.term}
                    placeholder="ابحث من خلال تاريخ الاستحقاق (اليوم - التاريخ - السنة)"
                    />
                </div> 

                <div className='title_div col-m-12 col-s-12 col-xs-12 '>
                <select 
                
                onChange={this.ChangeYear}
                value={this.state.year}
                >
                    {this.state.years && this.state.years.map((item,id)=>{
                        return(
                            <option value={item.year}>{item.year}</option>
                        )
                    })}

                </select>
                </div>

                </form>         
                
            <div className='row'>
            
   
               <div className='form_sub_div col-m-12 col-s-12 col-xs-12'> 
              
                   <table id="myTable">
                   <tr class="header">
                
                                    <th >الاسم</th>
                                    <th >تاريخ</th>
                                    <th >تاريخ  الاستحقاق</th>
                                    <th >السعر</th>
                                    <th> عرض</th>
                                    <th> تعديل</th>
                                    <th> حذف</th>

                                </tr>
                                <Modal show={this.state.show}
                                 handleClose={this.hideModal} 
                                 userEditType={this.state.userEditData} 
                                updatedUserResult={this.handlerUpdateUser} >
                                   </Modal>
                       
                                   {this.state.editScreen && <ResModal data={this.state.info}  deactive={this.deactive} ></ResModal>}
                                   {this.state.displayscreen && <ResDisplay data={this.state.info} deactive={this.deactiveDisplay}></ResDisplay>}

     {this.state.displyAllUsers&& this.state.displyAllUsers.filter(searchingFor(this.state.term)).map((info,idx)=>{
                           
                           if(idx >=start_offset && start_count <per_page){   
                               start_count++;
                           return(
                                  
                                
                               <tr>        
                               <td>{info.name}</td>
                               <td>{info.date}</td>
                               <td>{info.date_res}</td>
                               <td>{info.price}</td>
                               <td><button 
                               className="button_display"
                               onClick={()=>this.display(info)}>عرض</button></td>
                               <td><button 
                               className="button_edit"
                               onClick={()=>this.edit(info)}>تعديل</button></td>
                               <td><button 
                               className="button_delete"
                               onClick={()=>this.submit(info.id)}>حذف</button></td>

                                     </tr>
                                     
                               )
                           }
                                  })} 
                                

                    </table>

                    <Pagination className="users-pagination pull-right" bsSize="medium">
                                {this.state.itemPage.map((item,id)=>{
                                    return(
                                        item
                                    )
                                })}
                        
                    </Pagination>  
               </div> 
                 
           </div>    
            
         
   
   
         
            
                  
            </div>
           
           </div>
   
           </div>
           
        )
    }
}
const mapToprops=(state)=>{
    return{
  showOrcasReports:state.pass.orcasDailyReport ,
  showOrcasRevReport:state.pass.orcasResReport,
  years:state.pass.years
    }
}
const mapDispatch=dispatch=>{
    return{

        displayUser:()=>dispatch(editAllUser()),
        deleteUser:(deleteUserId)=>dispatch(deleteUser(deleteUserId)),
        getReportOrcas:(data)=>dispatch(get_dailyReportOrcas(data)),
        getReportResOrcas:(data)=>dispatch(get_ResReportOrcas(data)),
        deleteQualityRep:(id)=>dispatch(deleteResReport(id)),


        
    }
}


export default connect(mapToprops,mapDispatch)(PreviousRes)

