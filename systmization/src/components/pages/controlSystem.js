
import React, { Component } from 'react';
import Toolbar from "../Toolbar/Toolbar"
import {connect} from "react-redux"
import NewUser from "../forms/createNewUser"
import EditAndDelete from "./editAndDeleteuser"
import {createNewUser,insertRev, insertEva,insertEva_Edit} from "../actions/auth"
import {NotificationContainer, NotificationManager} from 'react-notifications';
import DailyOrcas from './dailyOrcasRe'
import CompanyRev from './companyRevReport'
import CompanyQuality from "./companyQualitRep"
import FormEva from '../forms/FormEva'
import FormRes from '../forms/formRes'
import QualityForm from '../forms/QualityForm'
import PreviousRes from './PreviousRes'



class controlSystem extends Component{
   
    state={
        value:this.props.location.pathname.substring(1,6),
        type:'',
        qualityUpdate:true,
    }
      

    handlerRequest=(value)=>{
        this.setState({
            type:value
        })
       // alert(value)
    }

    handlerFormAddUser=(data)=>{
       
console.log(data)

this.props.creatNewUser(data)
setTimeout(function() { 
    window.location.reload(); 
}.bind(this), 1000)
    }


handerSubmit=(data)=>{     
    this.props.revInsert(data)
    setTimeout(function() { 
        window.location.reload(); 
    }.bind(this), 1000)
        }

    handlerEvaSubmit=(data)=>{
        this.props.evaInsert(data);
        console.log(data)
        }

    render(){
        return(

             <div>
        
        

         <Toolbar  value= {this.props.userType} handlerType={this.handlerRequest} ></Toolbar>
             
         
          {this.state.type ==="createNewUser" ? <NewUser createNewUser={this.handlerFormAddUser}></NewUser>:<p1>exit</p1>}
          {this.state.type=="EditAndDelete" && <EditAndDelete></EditAndDelete>}
          
          {this.state.type ==="ViewOrcasReport" && <DailyOrcas reportTypes={this.state.type}></DailyOrcas>}
          {this.state.type ==="ViewBIMReport" && <DailyOrcas reportTypes={this.state.type}></DailyOrcas>}
          
          {this.state.type ==="ViewFutureReport" && <DailyOrcas reportTypes={this.state.type}></DailyOrcas>}
          
          {this.state.type ==="ViewDailyReportZuwer" && <DailyOrcas reportTypes={this.state.type}></DailyOrcas>}
          {this.state.type ==="ViewRevReportZuwer" && <CompanyRev reportTypes={this.state.type}></CompanyRev>}
          
          {this.state.type ==="ViewPreviousRevReportZuwer" && <PreviousRes reportTypes={this.state.type}></PreviousRes>}

          
          

          {this.state.type==="EnterViewQualityReportZuwer" && <QualityForm FormType={this.state.type} isNew={this.state.qualityUpdate}></QualityForm>}
          {this.state.type ==="ViewQualityReportZuwer" &&<CompanyQuality reportTypes={this.state.type}></CompanyQuality>}
            
           {this.state.type ==="addOrcasReport" && <FormEva  FormType={this.state.type} submit={this.handlerEvaSubmit}></FormEva>}
           {this.state.type ==="addBIMReport" && <FormEva  FormType={this.state.type} submit={this.handlerEvaSubmit}></FormEva>}
           {this.state.type ==="AddFutureReport" && <FormEva  FormType={this.state.type} submit={this.handlerEvaSubmit}></FormEva>}
           {this.state.type ==="EnterViewDailyReportZuwer" && <FormEva  FormType={this.state.type} submit={this.handlerEvaSubmit}></FormEva>}

           {this.state.type ==="EnterRevReportZuwer" && <FormRes FormType={this.state.type} submit={this.handerSubmit}></FormRes>}
           {/*this.state.type === "ُEnterViewQualityReportZuwer" && <FormQuality></FormQuality>*/}

           

          {this.props.CreatedUser ===true && NotificationManager.success( 'تم اضافة مستخدم جديد بنجاح','ان شاء مستخدم جديد' )}
          <NotificationContainer> </NotificationContainer>
    
                
              </div>



        )
        
        
        
    }
}
const mapToProps=state=>{
    return{
        userType:state.auth.types,
        CreatedUser:state.pass.CreatedUser,
        isInserted:state.pass.insertResult,
        ifInserted:state.evaluation.insertResult,
        data:state.evaluation_edit.eva_editData,
    }
}
const mapToDispatch=dispatch=>{
    return{
        creatNewUser:(data)=>dispatch(createNewUser(data)),
        revInsert:(data)=>dispatch(insertRev(data)),
        evaInsert:(data)=>dispatch(insertEva(data)),
        eva_editInsert:(data)=>dispatch(insertEva_Edit(data)),
    }
  
}


export default connect(mapToProps,mapToDispatch)(controlSystem)