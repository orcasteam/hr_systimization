import './FormRes.css';
import React,{Component} from 'react';
import {connect} from 'react-redux';
import {getAllRes_data,deleteRes} from '../actions/auth'
import { confirmAlert } from 'react-confirm-alert'; // Import
import ResModal from './resModal'
import ResDisplay from './resDisplay'

function searchingFor(term){
    
    return function(x){
        return x.name.toLowerCase().includes(term.toLowerCase())|| !term;
        
    }
}

class FormRes_edit extends Component{
    state={
        data:'',
        term:'',
        editScreen:false,
        info:'',
        noDate:'',
        displayscreen:false,
    }
    componentDidMount(){
        var d=this.props.username
        this.props.getAllRes_data(d)

        setTimeout(function() { 
            this.setState({data:this.props.data})

            if(this.props.data.length ===0){
                this.setState({noDate:false})
            }
            else{
                this.setState({noDate:true})
            }
        }.bind(this), 1000) 

       
    }
    handleSearchItem=(e)=>{
        this.setState({term:e.target.value})
    }
    handerDeleteItem=(id)=>{
        const newRes=this.state.data.filter(item=>{
            return item.id != id
        })
        this.setState({data:newRes})
        this.props.deleteRes(id)
       // alert(id)

    }
    submit=(id)=>{
        confirmAlert({
            title: 'تأكيد الحذف',

            buttons: [
              {
                label: 'نعم',
                
                onClick: () =>this.handerDeleteItem(id)
              },
              {
                label: 'لا',
              }
            ]
          })
    }

    display=(Info)=>{
        this.setState({info:Info})
        this.setState({displayscreen:true})
    }

    deactiveDisplay=(data)=>{

        this.setState({displayscreen:false})
    }

    
    showModal=(info)=>{
        this.setState({editScreen:true})
        this.setState({info:info})
    
    }
    
    deactive=(data)=>{
        
        this.setState({editScreen:false})
        if(data){
          
             this.state.data.map((item,id)=>{
               if(item.id == data.id){
                   console.log(data.id);
                    item.name=data.name
                    item.date=data.date
                    item.day=data.day
                    item.date_res=data.res_date
                    item.phone=data.phone
                    item.price=data.price
                    item.res_normal=data.normalRes
                    item.gar_res=data.garsone
                    item.resp_res=data.respons
                    item.res_day=data.res_day
                    item.res_number=data.res_number
                    item.res_time=data.res_time 
                    console.log(item.id);  
                }
            })
              
            
        }
    }

    render(){
        return(
            <div className='row'>
                {this.state.data &&
                   
                    <div className='main_div col-m-12 col-s-12 col-xs-12' >
           
                    <div className='tabel_Edit_div col-m-9 col-s-12 col-s-12'>

                          <form>
                    <div className='title_div col-m-12 col-s-12 col-xs-12 '>
                        <input type="text"
                        id="myInput" 
                        onChange={this.handleSearchItem}
                        value={this.state.term}
                        placeholder="ابحث عن اسم ..."
                        title="Type in a name"/>
                </div> 
                </form> 

                <div className='form_sub_div col-m-12 col-s-12 col-xs-12'> 
            
                <table id="myTable">
                <tr class="header">
                                    <th >الاسم</th>
                                    <th > التاريخ</th>
                                    <th >تاريخ الحجز</th>
                                    <th> عرض</th>
                                    <th> تعديل</th>
                                    <th> حذف</th>
                                </tr>
               
            {this.state.editScreen && <ResModal data={this.state.info}  deactive={this.deactive} ></ResModal>}
            {this.state.displayscreen && <ResDisplay data={this.state.info} deactive={this.deactiveDisplay}></ResDisplay>}
                    
            {this.state.data && this.state.data.filter(searchingFor(this.state.term)).map((reservation,id)=>{
                 return(
                <tr>
                    <td>{reservation.name}</td>
                    <td>{reservation.date}</td>
                    <td>{reservation.date_res}</td>

                     <td><button 
                    className="button_display"
                    onClick={()=>this.display(reservation)}
                    >
                    عرض</button></td>

                    <td><button 
                            onClick={()=>this.showModal(reservation)}
                            className="button_edit" 
                            >نعديل</button></td>
                    <td><button 
                            className="button_delete"
                            onClick={()=>this.submit(reservation.id)}
                            >
                            حذف</button></td>
                   
                                     
                </tr>
                 )
               
            })}

            

             </table>
             </div>  

            </div>
            </div>
                
            
                } 

                {this.state.noDate ===false && 
                    <div className='main_div col-m-12 col-s-12 col-xs-12' >

                    <form className='form_div col-m-9 col-s-12 col-s-12'   >
                    <div className='title_div col-m-12 col-s-12 col-xs-12 '> <h3> لم يتم اضافة حجوزات  من قبل هذا المستخدم </h3> </div> 
                    </form>
                                
                    </div>
                }
                

            </div>
        )
    }


}

const mapdispatch = dispatch =>{
    return {
        getAllRes_data:(data)=>dispatch(getAllRes_data(data)),
        deleteRes:(data)=>dispatch(deleteRes(data))
    }
}
const maptToProps=(state)=>{
    return{
       data:state.pass.allData,
       username:state.auth.username
    }
  }


export default connect(maptToProps,mapdispatch) (FormRes_edit)