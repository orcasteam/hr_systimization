
import './FormRes.css';
import React,{Component} from 'react';
import {insertEva_Edit ,delete_row, update_data} from '../actions/auth';
import {NotificationContainer, NotificationManager} from 'react-notifications';
//import Popup from "reactjs-popup";
import {connect} from 'react-redux';
import NewRow_edit from './newRow_edit'
import NewRow_edit2 from './newRow_edit2'
import InlineError from '../messages/inlinError'
import BIM from '../images/BIM.jpg'
import Orcas from '../images/ORCAS-Small.png'
import Zuwar from '../images/zuwar.jpg'
import Future from '../images/future.jpg'

var Loader = require('react-loader');

  


class FormEva_edit extends Component{
    state={
        data:{
            id:'', 
            shift_resp:'',
            shift:this.props.shift, 
            day:'', 
            date:'',
            emergency_state:'',
            notes:'',
            acheivement:'',
            type:'',
        },
        status:true,
        employeeAbsence:{},
        employeeLeft:{},
        employeeLate:{},
        formType:this.props.FormType,
        shiftRespClassName:'input_fields',
        shiftClassName:'input_fields',
        thereIsData:true,
        spinner:true,
        classNameForm:'form_div col-m-9 col-s-12 col-s-12',
        image:'',
        
    }

handerDelete=(id,type)=>{
       // this.preventDefault();
       if(type === 'absence'){
            const employeeabseceNew=this.state.employeeAbsence.filter(term=>{
            return term.id !=id
       });
       this.setState({employeeAbsence:employeeabseceNew})
        }
        if (type === 'late'){
            const employeeLateNew=this.state.employeeLate.filter(term=>{
                return term.id !=id
        });
        this.setState({employeeLate:employeeLateNew});
        }
        if (type === 'left'){
            const employeeLeftNew=this.state.employeeLeft.filter(term=>{
                return term.id !=id
        });
        this.setState({employeeLeft:employeeLeftNew});
        }
     
        this.props.delete_Row(id)
    }
handlerpartSubmit(e){
    e.preventDefault()
}
componentDidMount(){
    
    var d;
    var obj;

    if(this.state.formType=="zuwarDisplay" || this.state.formType=="EnterViewDailyReportZuwer"){
        d="zuwarEnter"
    }
    else if(this.state.formType=="orcasDisplay"|| this.state.formType=="addOrcasReport"){
        d="orcasEnter"
    }
    else if (this.state.formType=="FutureDisplay" || this.state.formType=="AddFutureReport"){
        d="FutureEnter"
    }
    else if (this.state.formType=="BIMDisplay" || this.state.formType=="addBIMReport"){
        d="BIMEnter"
    }
    
    if(this.props.isDisplay ===true){
        this.setState({classNameForm:'for_div_edit col-m-12 col-s-12 col-s-12'})

        obj={formType:this.props.info.type, shift:this.props.info.shift,username:this.props.info.shift_resp,id:this.props.info.id}
        //console.log(obj)
        this.props.eva_editInsert(obj);
    }
    else{
    var username=this.props.username;
    
    obj={formType:d, shift:this.props.shift ,username:username}
    this.props.eva_editInsert(obj);
    }


    setTimeout(function() { //Start the timer
 
    var gettingdata=this.props.data
    if(Object.keys(this.props.data).length > 0){
        const obj = {id:gettingdata[0].id, shift_resp:gettingdata[0].shift_resp, shift:gettingdata[0].shift, day:gettingdata[0].day, date:gettingdata[0].date
            ,emergency_state:gettingdata[0].emergency_state,notes:gettingdata[0].notes,acheivement:gettingdata[0].acheivement,type:gettingdata[0].type}
       
          this.setState({
            data: obj
        });
 
        if(gettingdata[0].type == 'zuwarEnter'){
            this.setState({image:Zuwar})
        }
        if(gettingdata[0].type == 'orcasEnter'){
            this.setState({image:Orcas})
        }
        if(gettingdata[0].type == 'FutureEnter'){
            this.setState({image:Future})
        }
        if(gettingdata[0].type == 'BIMEnter'){
            this.setState({image:BIM})
        }
    
        gettingdata.map((row,id)=>{
           if(row.type_em==='absence'){
              const obj={id:row.id_em ,id_table:row.id_table , name:row.name , reason :row.reason ,type:row.type_em}
            this.setState((prevState) => ({
                employeeAbsence: [...prevState.employeeAbsence,obj ],
            }));
        }
            if(row.type_em==='left'){
                const obj={id:row.id_em ,id_table:row.id_table , name:row.name , reason :row.reason,time:row.time ,type:row.type_em}
              this.setState((prevState) => ({
                  employeeLeft: [...prevState.employeeLeft,obj ],
              }));
            }
            if(row.type_em==='late'){
            const obj={id:row.id_em ,id_table:row.id_table , name:row.name , reason :row.reason,type:row.type_em}
            this.setState((prevState) => ({
                employeeLate: [...prevState.employeeLate,obj ],
            }));     
        }

        
        })
    }else{
        this.setState({thereIsData:false});
      
    }

    this.setState({status:false})

}.bind(this), 500) 
}

handleEmployeeAbsenceChange=(e)=>{
    if(["name","reason"].includes(e.target.className)){
        let employeeAbsence=[...this.state.employeeAbsence]
        employeeAbsence[e.target.dataset.id][e.target.className]=e.target.value
        this.setState({employeeAbsence},()=>{console.log(this.state.employeeAbsence)})
     
    }else{
    this.setState({[e.target.name]:e.target.value});

    }
}
handleEmployeeLateChange=(e)=>{
    if(["name","reason"].includes(e.target.className)){
        let employeeLate=[...this.state.employeeLate]
        employeeLate[e.target.dataset.id][e.target.className]=e.target.value
        this.setState({employeeLate},()=>{console.log(this.state.employeeLate)})

    }else{
    this.setState({[e.target.name]:e.target.value});

    }
}
handleEmployeeLeftChange=(e)=>{
    if(["name","reason","time"].includes(e.target.className)){
        let employeeLeft=[...this.state.employeeLeft]
        employeeLeft[e.target.dataset.id][e.target.className]=e.target.value
        this.setState({employeeLeft},()=>{console.log(this.state.employeeLeft)})
       
    }else{
    this.setState({[e.target.name]:e.target.value});
 
    }
}

handerlAddEmployeeAbsence = (e) => {
    e.preventDefault();
    this.setState((prevState) => ({
        employeeAbsence: [...prevState.employeeAbsence, {id_em:null,id_table:this.state.data.id,name:"", reason:"",type:'absence'}],
    }));
}
handerlAddEmployeeLate = (e) => {
    e.preventDefault();
    this.setState((prevState) => ({
        employeeLate: [...prevState.employeeLate, {id_em:null,id_table:this.state.data.id,name:"", reason:"",type:'late'}],
    }));
}
handerlAddEmployeeLeft = (e) => {
    e.preventDefault();
    this.setState((prevState) => ({
        employeeLeft: [...prevState.employeeLeft, {id_em:null,id_table:this.state.data.id,name:"", reason:"",time:"",type:'left'}],
    }));
}

handlenotesChange=(e)=>{
    this.setState({data:{...this.state.data , notes:e.target.value}})

}
handleEmergencyChange=(e)=>{
    this.setState({data:{...this.state.data , emergency_state:e.target.value}})
 
}
handleAchevChange=(e)=>{
    this.setState({data:{...this.state.data , acheivement:e.target.value}})
  

}

handleSubmit=(e)=>{
    e.preventDefault();

    this.setState({spinner:false});
    this.props.update_Data(this.state)
    if(this.props.isDisplay=== true){
        this.props.deactive()
    }
    setTimeout(function() { //Start the timer
        this.setState({spinner: true}) //After 1 second, set render to true
    }.bind(this), 1000)


}


    render(){

        return( 
    
    <div className='row' id='convertTopdf'>
    {(this.state.status===false  && this.props.data.length>0)  &&
    <div className='main_div col-m-12 col-s-12 col-xs-12'  >

       <form className={this.state.classNameForm} onSubmit={this.handleSubmit} >
       <div className='title_div col-m-12 col-s-12 col-xs-12 '> <img src={this.state.image} style={{width:150, height:150  }}/> </div>      
       <div className='title_div col-m-12 col-s-12 col-xs-12 '> <h1>تقييم اليوم </h1> </div> 


           <div className='form_sub_div col-m-12 col-s-12 col-xs-12'>  

                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                        <div className='col-m-4 col-s-4 col-xs-4'> 
                                <label className='label_fields '>مسؤول الوردية</label> </div> 

                        <div className='col-m-8 col-s-8 col-xs-8'>
                        
                        <input  className={this.state.shiftRespClassName}
                            value={this.state.data.shift_resp} 
                            name='shiftResp'
                            onChange={this.handleShiftRespChange}
                            ></input>            
                                <InlineError></InlineError>
                                </div>
                </div>


                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    <div className='col-m-4 col-s-4 col-xs-4'>
                        <label className='label_fields' >الوردية</label> </div>
                    
                    <div className='col-m-8 col-s-8 col-xs-8'>

                    <div className='col-m-8 col-s-8 col-xs-8'>
                    
                    <input  className={this.state.shiftClassName}
                        value={this.state.data.shift} 
                        name='shift'
                        
                    ></input>            
                        <InlineError></InlineError>
                        </div>

                    
                    </div>
        
                </div>

                  <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                <div className='col-m-4 col-s-4 col-xs-4'> 
                        <label className='label_fields '> اليوم</label> </div> 

                   <div className='col-m-8 col-s-8 col-xs-8'>
                        <label className='input_fields'> {this.state.data.day}</label> </div>
           
                </div>
                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                        <div className='col-m-4 col-s-4 col-xs-4'> 
                                <label className='label_fields '>التاريخ </label> </div> 

                            <div className='col-m-8  col-s-4 col-xs-4'>
                                <label className='input_fields '>{this.state.data.date}</label> </div>  
                </div>

            </div>

            <div className='form_sub_div col-m-12 col-s-12 col-xs-12'>  
            <div className='flex_start col-m-12 col-xs-12 col-s-12'>
                <label className='label_fields'><strong>الموظفين الغائبين</strong></label>
            </div>        
       
            <div className='flex_start col-m-12 col-xs-12 col-s-12'>
                <button className='button_fields ' onClick={this.handerlAddEmployeeAbsence} >+</button> </div> 
                   
            </div>
                {this.state.employeeAbsence.length>0 &&
                <form onChange={this.handleEmployeeAbsenceChange} onSubmit={this.handlerpartSubmit.bind(this)}>
                <NewRow_edit name={this.state.employeeAbsence}
                    delete={this.handerDelete}
                ></NewRow_edit>
                </form>
                }

            <div className='form_sub_div col-m-12 col-s-12 col-xs-12'>  
                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    <label className='label_fields'><strong>الموظفين المتاخرين عن عملهم</strong></label></div>
        

            <div className='flex_start col-m-12 col-xs-12 col-s-12'>
                <button className='button_fields ' onClick={this.handerlAddEmployeeLate}>+</button> </div> 
            </div>
                    {this.state.employeeLate.length >0 &&
                    <form onChange={this.handleEmployeeLateChange} onSubmit={this.handlerpartSubmit.bind(this)}>
                        <NewRow_edit name={this.state.employeeLate}
                         delete={this.handerDelete}
                        ></NewRow_edit>
                    </form>
                    }
            <div className='form_sub_div col-m-12 col-s-12 col-xs-12'>  
                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    <label className='label_fields'><strong>اذن المغادرة</strong></label></div>
            
                <div className='flex_start col-m-12 col-xs-12 col-s-12'>
                        <button className='button_fields ' onClick={this.handerlAddEmployeeLeft} >+</button> </div> 
                </div>
                        {this.state.employeeLeft.length>0 &&
                        <form onChange={this.handleEmployeeLeftChange} onSubmit={this.handlerpartSubmit.bind(this)}>
                            <NewRow_edit2 name={this.state.employeeLeft}
                             delete={this.handerDelete}
                            ></NewRow_edit2>
                        </form>
                        }


               <div className='row'>
        <div className='form_sub_div col-m-12 col-s-12 col-xs-12'> 
            <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                <div className='col-m-4 col-s-4 col-xs-4'>        
                    <label className='label_fields'>الحالات الطارئة </label></div>
                <div className='col-m-4 col-s-8 col-xs-8'>
                    <textarea type='text' className='textarea' 
                    onChange={this.handleEmergencyChange}
                    name='emergency_state'
                    value={this.state.data.emergency_state}
                    ></textarea></div>
            </div>

             <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                <div className='col-m-4 col-s-4 col-xs-4'>        
                    <label className='label_fields'> الملاحظات </label></div>
                <div className='col-m-4 col-s-8 col-xs-8'>
                    <textarea type='text' className='textarea'
                    onChange={this.handlenotesChange}
                    name='notes'
                    value={this.state.data.notes}
                    ></textarea></div>
            </div>    
          
        </div>
    </div>


     <div className='row'>
        <div className='form_sub_div col-m-12 col-s-12 col-xs-12'> 
            <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                {this.props.isDisplay===true && 
                  <button className='button_style'>تعديل التقييم </button> 
                  }
                
            </div>
        </div>
    </div>

     <div className='row'>
     {this.props.isDisplay !== true &&
        <div className='button_div col-m-12 col-s-12 col-xs-12 '> 
           
            {this.state.spinner?<button className='button_style'>تعديل التقييم </button> :
                <Loader top='auto' left='auto'></Loader>
                }
            {this.state.spinner=== false &&  NotificationManager.success( 'تم تعديل تقييم بنجاح','تعديل التقييم' )}
            <NotificationContainer/>



        </div> 
     }         
    </div>  
        
     </form>
    
    </div>
    }

    {this.state.thereIsData===false &&  <div className='main_div col-m-12 col-s-12 col-xs-12' >
    
    <form className='form_div col-m-9 col-s-12 col-s-12'   >
       <div className='title_div col-m-12 col-s-12 col-xs-12 '> <h3> لم يتم اضافة تقييم لهذا اليوم </h3> </div> 
    </form>
                
    </div>
    }
    </div>
        
        );
    }
}


const mapdispatch =dispatch =>{
    return{
        eva_editInsert:(data)=>dispatch(insertEva_Edit(data)),
        delete_Row:(data)=>dispatch(delete_row(data)),
        update_Data:(data)=>dispatch(update_data(data)),
    }
}
const maptToProps=(state)=>{
    return{
        data:state.evaluation_edit.eva_editData,
        username:state.auth.username
    }
  }

export default connect(maptToProps,mapdispatch)(FormEva_edit)
