import './FormRes.css';
import React,{Component} from 'react';
import {allQualityInsert} from '../actions/auth';
import {connect} from 'react-redux';
import Zuwar from '../images/zuwar.jpg'

var jsPDF =require('jspdf')
var html2canvas=require('html2canvas')

var today = new Date();
class QualityDisplay extends Component{

    
    state={
        note:'',
        name:'',
        date:today.getFullYear()+'-'+ (today.getMonth()+1)+'-'+today.getDate(),
        isAllow:true,
        formDisplay:'form_div_display',
    }

    componentDidMount(){
        if(this.props.isAdmin !== true){
        var obj={name:this.props.username , date:this.state.date}
        this.props.allQualityInsert(obj);
       
        setTimeout(function(){
            //console.log(this.props.allQuality)
            if(this.props.allQuality){
            this.setState({note:this.props.allQuality[0].note})
            this.setState({date:this.props.allQuality[0].date})
            this.setState({name:this.props.allQuality[0].name})
        }
            
        }.bind(this),500)

    }
    else{

        this.setState({note:this.props.data.note})
        this.setState({name:this.props.data.name})
        this.setState({ date:this.props.data.date}) 
        this.setState({formDisplay:'form_div_display_admin'})

    }
    }

    submit=(e)=>{
        e.preventDefault();
        const input=document.getElementById('convertTopdf')
    
        
        this.setState({isAllow:false})
        html2canvas(input).then((canvas)=>{
            
            
            const imgData = canvas.toDataURL('image/png');
            const pdf = new jsPDF('p', 'mm', [297, 210]);
        
            pdf.addImage(imgData, 'JPEG',0,0);
        
        
            window.open(pdf.output('bloburl'), '_blank');
    })
    setTimeout(function(){
        this.setState({isAllow:true});
    }.bind(this),200)
    
    }
    render(){
        return(
            <div>
                {(this.props.allQuality || this.props.isAdmin===true )?
               
               <div className="main_div col-m-12 col-s-12 col-xs-12">

               <form className={this.state.formDisplay} onSubmit={this.submitForm} id="convertTopdf">

              
                    <div className="mainFormDisplay">
                    <div className='title_div col-m-12 col-s-12 col-xs-12 '> <img src={Zuwar} style={{width:120, height:110}}/> </div>   
                        <div className='title_div col-m-12 col-s-12 col-xs-12 '> <h1> تقرير الجودة </h1> </div> 

                            <div className='displaycontentDiv col-m-12 col-s-12 col-xs-12'>
                                <div className='DisplayLabel3 col-m-12'>
                                        <label>   التاريخ &nbsp; {this.state.date}</label>
                                        <span className='spanDisplay' style={{ color: "#fff",marginRight:'58px' ,marginTop:'-100px'}} > -------------------</span>
                                </div>
                        
                                <div className='DisplayLabel3 col-m-12'>
                                        <label>   الاسم &nbsp; {this.state.name}</label>
                                        <span className='spanDisplay' style={{ color: "#fff",marginRight:'53px' ,marginTop:'-100px'}} > -------------------</span>
                                </div>
                            </div>

                            <div className='DisplayLabel3 col-m-12 col-s-12 col-xs-12'>
                                <label>   الملاحظات &nbsp; </label>
                                <p className='paraDisplay'>{this.state.note}</p>
                            </div>

                        
                        {this.state.isAllow===false &&

                            <div className='DisplayLabel5 col-m-12 col-s-12 col-xs-12'>
                                <label>   التوقيع &nbsp; </label>
                            </div>
                        }

                        {this.state.isAllow===true &&
                        <div className="DisplayLabel4 col-m-12">
                        <button onClick={this.submit} className="buttonDisplay1"><img className='printImg'></img>طباعة</button>
                        </div>
                        
                        }

                    </div>
                </form>
                </div>
                :
                <div className='main_div col-m-12 col-s-12 col-xs-12' >
    
                    <form className='form_div col-m-9 col-s-12 col-s-12'   >
                    <div className='title_div col-m-12 col-s-12 col-xs-12 '> <h3> لم يتم اضافة تقرير الجودة لهذا اليوم </h3> </div> 
                    </form>
                            
                </div>

                
                }
            
            </div>
        )
    }

}

const mapdispatch = dispatch =>{
    return {
       allQualityInsert:(data)=>dispatch(allQualityInsert(data))
    }
}
const maptToProps=(state)=>{
    return{
        username:state.auth.username,
       allQuality:state.pass.allData,
    }
  }


export default connect(maptToProps,mapdispatch) (QualityDisplay)