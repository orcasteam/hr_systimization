
import './FormRes.css';
import React,{Component} from 'react';




class newRow_edit extends Component{

    handleonclick=(value,type)=>{
        this.props.delete(value,type)
    }
    
    render(){

        
    return(             
        
            this.props.name.map((val,idx)=>{
             
                let employeeId=`employee-${idx}`
                let resonId=`res-${idx}`
              //  let id_button={val.id}
                
                return(


                    <div className='row' key={idx}>
                     <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    <div className='col-m-3 col-s-4 col-xs-4'>
                        <label
                         className='label_fields'
                         htmlFor={employeeId}
                          >{`الاسم # ${idx+1}`} </label> </div>  
                
                    <div className='input_container col-m-5 col-s-8 col-xs-5'> 
                         <input 
                         type="text"
                          className='name'
                          name={employeeId}
                          data-id={idx}
                          id={employeeId}
                          value={val.name}
                       
                          ></input> </div>
                    <div className='col-m-4 col-s-1 col-xs-4'></div>
                    
                </div>  

                  <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    <div className='col-m-3 col-s-4 col-xs-4'> 

                             <label className='label_fields'>   السبب </label></div>

                    <div className='input_container col-m-5 col-s-8 col-xs-5'> 
                            <input 
                            type="text"
                             className='reason'
                             name={resonId}
                             data-id={idx}
                             id={resonId}
                             value={val.reason}
                            ></input></div>

                    <div className='col-m-4 col-s-1 col-xs-4'>
                    
                        <button className='Deletebutton'
                        onClick={()=>this.handleonclick(val.id,val.type)}
                        >حذف</button></div>

                             </div>
                    </div>       
                   
                  
                

                )


            })
          
        
    )
}

}

export default newRow_edit