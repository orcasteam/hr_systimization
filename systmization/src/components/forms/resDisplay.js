import './FormRes.css';
import im from '../images/zuwar.jpg'
import React,{Component} from 'react';
var jsPDF =require('jspdf')
var html2canvas=require('html2canvas')


class resDisplay extends Component{
    state={
        isallow:true,
    }

    handleDisplay(x){
        this.props.deactive()
    }
    submitForm=(e)=>{
        e.preventDefault();
        this.setState({isallow:false})
        const input=document.getElementById('convertTopdf')
    
   
        html2canvas(input).then((canvas)=>{
             
            const imgData = canvas.toDataURL('image/jpeg');
            const pdf = new jsPDF('p', 'mm', [297, 210]);
        
            pdf.addImage(imgData, 'JPEG',0,0);
        
        
            window.open(pdf.output('bloburl'), '_blank');
    })
   // this.props.deactive()
   setTimeout(function(){
    this.setState({isallow:true});
}.bind(this),200)

}
    render(){
        return(
            <div className='popup'>
            <button  className="closeButton" onClick={()=>this.handleDisplay("welcome")}>x</button>
            <div className='popup-inner_display'>
            <form className="resDisplay_form" id="convertTopdf">

                <div className='title_div col-m-12 col-s-12 col-xs-12 '> <img src={im} style={{width:150, height:150  }}/> </div>      
                <div className='title_div col-m-12 col-s-12 col-xs-12 '> <h1> تقرير الحجز </h1> </div> 
              

                <div className='displaycontentDiv col-m-12 col-s-12 col-xs-12'>
                    <div className='DisplayLabel3 col-m-12'>
                            <div style={{display:'flex', flex:'space-between'}}> <label style={{fontSize:25 , fontStyle:'bold'}}> التاريخ &nbsp;:</label> <label>&nbsp;{this.props.data.date}</label></div>
                            <span className='spanDisplay' style={{ color: "#fff",marginRight:'75px' ,marginTop:'-100px'}} > -------------------</span>
                    </div>
               
                    <div className='DisplayLabel3 col-m-12'>
                            <div style={{display:'flex', flex:'space-between'}}> <label style={{fontSize:25 , fontStyle:'bold'}}> اليوم &nbsp; : </label> <label>&nbsp;{this.props.data.day}</label></div>
                            <span className='spanDisplay' style={{ color: "#fff",marginRight:'65px' ,marginTop:'-100px'}} > -------------------</span>
                    </div>
                </div>
                
                <div className='displaycontentDiv col-m-12 col-s-12 col-xs-12'>
                    <div className='DisplayLabel3 col-m-12'>
                            <div style={{display:'flex', flex:'space-between'}}> <label style={{fontSize:25 , fontStyle:'bold'}}> الاسم &nbsp; : </label> <label>&nbsp;{this.props.data.name}</label></div>
                            <span className='spanDisplay' style={{ color: "#fff",marginRight:'60px' ,marginTop:'-100px'}} > -------------------</span>
                    </div>
                    <div className='DisplayLabel3 col-m-12'>
                            <div style={{display:'flex', flex:'space-between'}}> <label style={{fontSize:25 , fontStyle:'bold'}}> رقم الهاتف &nbsp;:</label> <label>&nbsp;{this.props.data.phone}</label></div>
                            <span className='spanDisplay' style={{ color: "#fff",marginRight:'100px' ,marginTop:'-100px'}} > -------------------</span>
                    </div>
                </div>

                <div className='displaycontentDiv col-m-12 col-s-12 col-xs-12'>
                    <div className='DisplayLabel3 col-m-12'>
                            <div style={{display:'flex', flex:'space-between'}}> <label style={{fontSize:25 , fontStyle:'bold'}}>مسؤول الحجز &nbsp;:</label> <label>&nbsp;{this.props.data.resp_res}</label></div>
                            <span className='spanDisplay' style={{ color: "#fff",marginRight:'125px' ,marginTop:'-100px'}} > -------------------</span>
                    </div>
                    <div className='DisplayLabel3 col-m-12'>
                            <div style={{display:'flex', flex:'space-between'}}> <label style={{fontSize:25 , fontStyle:'bold'}}>السفرجي المسؤول &nbsp;:</label> <label>&nbsp;{this.props.data.gar_res}</label></div>
                            <span className='spanDisplay' style={{ color: "#fff",marginRight:'170px' ,marginTop:'-100px'}} > -------------------</span>
                    </div>
                </div>

              <div className='displaycontentDiv col-m-12 col-s-12 col-xs-12'>
                    <div className='DisplayLabel3 col-m-12'>
                            <div style={{display:'flex', flex:'space-between'}}> <label style={{fontSize:25 , fontStyle:'bold'}}>تاريخ الحجز &nbsp;:</label> <label>&nbsp;{this.props.data.date_res}</label></div>
                            <span className='spanDisplay' style={{ color: "#fff",marginRight:'115px' ,marginTop:'-100px'}} > -------------------</span>
                    </div>
                    <div className='DisplayLabel3 col-m-12'>
                    <div style={{display:'flex', flex:'space-between'}}> <label style={{fontSize:25 , fontStyle:'bold'}}>اليوم &nbsp;:</label> <label>&nbsp;{this.props.data.res_day}</label></div>
                            <span className='spanDisplay' style={{ color: "#fff",marginRight:'60px' ,marginTop:'-100px'}} > -------------------</span>
                    </div>
                </div>

                <div className='displaycontentDiv col-m-12 col-s-12 col-xs-12'>
                    <div className='DisplayLabel3 col-m-12'>
                            <div style={{display:'flex', flex:'space-between'}}> <label style={{fontSize:25 , fontStyle:'bold'}}>الساعة &nbsp;:</label> <label>&nbsp;{this.props.data.res_time}</label></div>
                            <span className='spanDisplay' style={{ color: "#fff",marginRight:'70px' ,marginTop:'-100px'}} > -------------------</span>
                    </div>
                    <div className='DisplayLabel3 col-m-12'>
                            <div style={{display:'flex', flex:'space-between'}}> <label style={{fontSize:25 , fontStyle:'bold'}}>عدد الاشخاص &nbsp;:</label> <label>&nbsp;{this.props.data.res_number}</label></div>
                            <span className='spanDisplay' style={{ color: "#fff",marginRight:'135px' ,marginTop:'-100px'}} > -------------------</span>
                    </div>
                </div>

                <div className='displaycontentDiv col-m-12 col-s-12 col-xs-12'>
                    <div className='DisplayLabel3 col-m-12'>
                            <div style={{display:'flex', flex:'space-between'}}> <label style={{fontSize:25 , fontStyle:'bold'}}>السعر &nbsp; : </label> <label>&nbsp;{this.props.data.price}</label></div>
                            <span className='spanDisplay' style={{ color: "#fff",marginRight:'70px' ,marginTop:'-100px'}} > -------------------</span>
                    </div>
                </div>
                
                <div className='displaycontentDiv col-m-12 col-s-12 col-xs-12'>

                     <div className='DisplayLabel3 col-m-12'>
                           <div> <label style={{fontSize:25 , fontStyle:'bold'}} > طبيعة الحجز&nbsp;:</label> 
                                <label style={{width:300 ,marginRight:130 , marginTop:-25 , marginBottom:30}}> {this.props.data.res_normal}</label>
                           </div>
                    </div>

                </div>
                <div className='title_div col-xs-12'>
                {this.state.isallow===true && <button onClick={this.submitForm} className='buttonDisplay1'>طباعة</button>
                   }
                {this.state.isallow===false &&

                    <div className='DisplayLabel5 col-m-12 col-s-12 col-xs-12'>
                        <label>   التوقيع &nbsp; </label>
                    </div>
                    }
                </div>
                    </form>

                
                </div>
            </div>
            
        )
    }

}

export default resDisplay