import './FormRes.css';
import React,{Component} from 'react';
import FormEvaDisplay from './formEvaDisplay'
import FormEvaEdit from './formEva_edit'

class formEvaAdminDisplay extends Component{

    handleDisplay=(data)=>{
        this.props.deactive()
    }
    render(){
        return(
            <div className='popup'>
            <button  className="closeButton" onClick={()=>this.handleDisplay("welcome")}>x</button>

            <div className='popup-inner_display'>
                {this.props.isDisplay === true && 
                <FormEvaDisplay info={this.props.data} isDisplay={this.props.isDisplay} deactive={()=>this.handleDisplay("welcome")}></FormEvaDisplay>    
                }
                {this.props.isEdit === true && 
                <FormEvaEdit info={this.props.data} isDisplay={this.props.isEdit} deactive={()=>this.handleDisplay("welcome")}></FormEvaEdit>
                }

            </div>
            
            </div>
        )
    }


}

export default formEvaAdminDisplay
