import './FormRes.css';
import React,{Component} from 'react';
import {insertEva_Edit ,delete_row, update_data} from '../actions/auth';
import {connect} from 'react-redux';
import BIM from '../images/BIM.jpg'
import Orcas from '../images/ORCAS-Small.png'
import Zuwar from '../images/zuwar.jpg'
import Future from '../images/future.jpg'
var jsPDF =require('jspdf')
var html2canvas=require('html2canvas')

class FormEvaDisplay extends Component{
    state={
        data:{
            id:'', 
            shift_resp:'',
            shift:this.props.shift, 
            day:'', 
            date:'',
            emergency_state:'',
            notes:'',
             acheivement:'',
            type:''
        },
        status:true,
        employeeAbsence:'',
        employeeLeft:'',
        employeeLate:'',
        formType:this.props.FormType,
        isAllow:true,
        classNameOfMain:'form_div_display',
        image:'',
    }



    componentDidMount(){
        var username;
        var d;
        var obj;

        if(this.props.isDisplay ===true){
            this.setState({classNameOfMain:'form_div_display_admin'})
            obj={formType:this.props.info.type, shift:this.props.info.shift,username:this.props.info.shift_resp,id:this.props.info.id}
            //console.log(obj)
            this.props.eva_editInsert(obj);

        }
        else {

        username=this.props.username;
        console.log(this.state.formType)
        if(this.state.formType=="zuwarEdit" || this.state.formType=="EnterViewDailyReportZuwer"){
            d="zuwarEnter"
            this.setState({image:Zuwar})
        }
        else if(this.state.formType=="orcasEdit"|| this.state.formType=="addOrcasReport"){
            d="orcasEnter"
            this.setState({image:Orcas})
        }
        else if (this.state.formType=="FutureEdit" || this.state.formType=="AddFutureReport"){
            d="FutureEnter"
            this.setState({image:Future})
        }
        else if (this.state.formType=="BIMEdit" || this.state.formType=="addBIMReport"){
            d="BIMEnter"
            this.setState({image:BIM})
        }



        obj={formType:d, shift:this.state.data.shift ,username:username}
        this.props.eva_editInsert(obj);
        }

        
        
        setTimeout(function() { //Start the timer
           //After 1 second, set render to true
        
        var gettingdata=this.props.data
        
        if(Object.keys(gettingdata).length > 0){
            const obj = {id:gettingdata[0].id, shift_resp:gettingdata[0].shift_resp, shift:gettingdata[0].shift, day:gettingdata[0].day, date:gettingdata[0].date
                ,emergency_state:gettingdata[0].emergency_state,notes:gettingdata[0].notes,acheivement:gettingdata[0].acheivement,type:gettingdata[0].type}
           
              this.setState({
                data: obj
            });

           

            if(gettingdata[0].type == 'zuwarEnter' ){
                this.setState({image:Zuwar})
            }
            if(gettingdata[0].type == 'orcasEnter'){
                this.setState({image:Orcas})
            }
            if(gettingdata[0].type == 'FutureEnter'){
                this.setState({image:Future})
            }
            if(gettingdata[0].type == 'BIMEnter'){
                this.setState({image:BIM})
            }
        
            gettingdata.map((row,id)=>{
               if(row.type_em==='absence'){
                  const obj={id:row.id_em ,id_table:row.id_table , name:row.name , reason :row.reason ,type:row.type_em}
                this.setState((prevState) => ({
                    employeeAbsence: [...prevState.employeeAbsence,obj ],
                }));
            }
                if(row.type_em==='left'){
                    const obj={id:row.id_em ,id_table:row.id_table , name:row.name , reason :row.reason ,time:row.time,type:row.type_em}
                  this.setState((prevState) => ({
                      employeeLeft: [...prevState.employeeLeft,obj ],
                  }));
                }
                if(row.type_em==='late'){
                const obj={id:row.id_em ,id_table:row.id_table , name:row.name , reason :row.reason,type:row.type_em}
                this.setState((prevState) => ({
                    employeeLate: [...prevState.employeeLate,obj ],
                }));     
            }
    
            
            })
        }else{
            this.setState({thereIsData:false});
          
        }
    
        this.setState({status:false})
   

        
    }.bind(this), 500) 
}

    submitForm=(e)=>{
        e.preventDefault();
        const input=document.getElementById('convertTopdf')
        this.setState({isAllow:false})
   
        html2canvas(input).then((canvas)=>{
            
            
            const imgData = canvas.toDataURL('image/png');
            const pdf = new jsPDF('p', 'mm', [297, 210]);
        
            pdf.addImage(imgData, 'JPEG',0,0);
        
        
            window.open(pdf.output('bloburl'), '_blank');
    })

    setTimeout(function(){
        this.setState({isAllow:true});
        if(this.props.isDisplay===true){
            this.props.deactive();
        }
    }.bind(this),200)
   
}

    render(){
        return(

            <div className="main_div col-m-12 col-s-12 col-xs-12">

               

               <form className={this.state.classNameOfMain} onSubmit={this.submitForm} id="convertTopdf">
              
               <div className='title_div col-m-12 col-s-12 col-xs-12 '> <img src={this.state.image} style={{width:150, height:150  }}/> </div>      

                    <div className="mainFormDisplay">
                        
                        
                        <div className="DisplayHeaderBox" >
                        <label> مسؤول الوردية &nbsp; {this.state.data.shift_resp}</label>
                        <label>الوردية &nbsp; {this.state.data.shift}</label>
                        <label>اليوم  &nbsp;{this.state.data.day}</label>
                        <label>التاريخ &nbsp;  {this.state.data.date}</label>
                        </div>


                        <div className='row'>
                        <div className='DisplayBodyBox'>
                            <label style={{fontSize:23 , fontWeight:'bold',}}> الموظفين الغائبين  &nbsp;: </label>
                       
                        <div className='DisplayBody1Box'>
                            {this.state.employeeAbsence.length >0  && 
                                this.state.employeeAbsence.map((employee,id)=>{

                                    return(
                                        <div className='DisplayBody2Box'>

                                        <div className='DisplayLabel col-m-6'>
                                        <label> الاسم &nbsp;: {employee.name}</label>
                                        <span  className='spanDisplay' style={{ color: "#fff",marginRight:'60px'} } > ---------------------</span>
                                         </div>
                                        <div className='DisplayLabel col-m-6'>
                                        <label>السبب  &nbsp;: {employee.reason}</label> 
                                        <span  className='spanDisplay' style={{ color: "#fff",marginRight:'50px'}} > ---------------------</span>
                                        </div>
                                        </div>
                                    )
                                })}
                        </div>   
                        </div>
                    


                    <div className='row'>
                        <div className='DisplayBodyBox'>
                            <label  style={{fontSize:23 , fontWeight:'bold',}}> الموظفين المتاخرين  &nbsp;: </label>
                       
                        <div className='DisplayBody1Box'>
                            {this.state.employeeLate.length >0  && 
                                this.state.employeeLate.map((employee,id)=>{

                                    return(
                                        <div className='DisplayBody2Box'>
                                        <div className='DisplayLabel col-m-6'>
                                        <label> الاسم &nbsp;: {employee.name}</label>
                                        <span className='spanDisplay' style={{ color: "#fff",marginRight:'60px'}} > ---------------------</span>
                                         </div>
                                        <div className='DisplayLabel col-m-6'>
                                        <label>السبب &nbsp;: {employee.reason}</label>
                                        <span className='spanDisplay' style={{ color: "#fff",marginRight:'50px'}} > ---------------------</span>
                                        </div>
                                        </div>
                                    )
                                })}
                        </div>   
                        </div>
                    </div>

                      <div className='row'>
                        <div className='DisplayBodyBox' style={{marginRight:-17}}>
                            <label  style={{fontSize:23 , fontWeight:'bold',}}> اذن المغادرة &nbsp;: </label>
                       
                        <div className='DisplayBody1Box'>
                            {this.state.employeeLeft.length >0  && 
                                this.state.employeeLeft.map((employee,id)=>{

                                    return(
                                        <div className='DisplayBody2Box'>
                                        <div className='DisplayLabel col-m-4'>
                                        <label> الاسم &nbsp;: {employee.name}</label> 
                                        <span className='spanDisplay' style={{ color: "#fff",marginRight:'60px'}} > ------------------</span>
                                        </div>
                                        <div className='DisplayLabel col-m-4'>
                                        <label>السبب &nbsp;: {employee.reason}</label>
                                        <span className='spanDisplay' style={{ color: "#fff",marginRight:'50px' ,marginTop:'-100px'}} > ------------------</span>
                                        </div>

                                        <div className='DisplayLabel col-m-4'>
                                        <label>الساعة &nbsp;: {employee.time}</label>
                                        <span className='spanDisplay' style={{ color: "#fff",marginRight:'50px' ,marginTop:'-100px'}} > ------------------</span>
                                        </div>
                                        </div>
                                    )
                                })}
                        </div>   
                        </div>

                    </div>
                                
                        <div className='DisplayLabel3 col-m-12'>
                            <label style={{fontSize:23 , fontWeight:'bold',}}> الحالات الطارئة  &nbsp;:</label> <label style={{marginRight:150 , marginTop:-30 , width:300}}> {this.state.data.emergency_state}</label>
                        </div>

                         <div className='DisplayLabel3 col-m-12'>
                            <label style={{fontSize:23 , fontWeight:'bold',}}>  الملاحظات &nbsp;:</label> <label style={{marginRight:110 , marginTop:-30 , width:300}}> {this.state.data.notes}</label>
                        </div>

                        <div className="DisplayLabel4 col-m-12" style={{marginRight:-40}}>
                            <label style={{fontSize:23 , fontWeight:'bold',}}>  التقييم العام للوردية &nbsp;:</label>
                            <label>ضعيف</label>
                            <label>جيد</label>
                            <label>جيد جدا</label>
                        </div>

                        
                        <div className="DisplayLabel3 col-m-12">
                            <label style={{fontSize:23 , fontWeight:'bold',}}> التوقيع &nbsp;:</label>
                        </div>

                        {this.state.isAllow===true &&
                        <div className="DisplayLabel4 col-m-12">
                        <button onClick={this.submitForm} className="buttonDisplay1"><img className='printImg'></img>طباعة</button>
                        </div>
                        
                        }
                     

                    </div>

                </div> 

                
                        
         
               </form>

            </div>

            
        )
    }
}


const mapdispatch =dispatch =>{
    return{
        eva_editInsert:(data)=>dispatch(insertEva_Edit(data)),
    
    }
}
const maptToProps=(state)=>{
    return{
        data:state.evaluation_edit.eva_editData,
        username:state.auth.username
    }
  }

export default  connect(maptToProps,mapdispatch)(FormEvaDisplay)