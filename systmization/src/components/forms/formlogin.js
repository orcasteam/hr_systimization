import React, { Component } from 'react';
import InlineError from '../messages/inlinError'

class FormLogin extends Component {
    constructor() {
        super();

        this.state = {
           data:{ 
               id: '',
            password: '',
           },
            errors:{},
            loading:false,
            
        };
    }

    handleChange=(e)=>{
        this.setState({
      data: {...this.state.data, [e.target.name]:e.target.value},
        })
    }
    hnadlersubmit=(e)=>{
        e.preventDefault();
       
        const errors=this.handlerValidator(this.state)
        this.setState({ errors });
        if(Object.keys(errors).length ===0 ){
          this.props.submit(this.state.data)  
        }
    }

    handlerValidator=(state)=>{
        const errors={};
        if(!this.state.data.password) errors.password="please Enter the password ";
       // if(this.state.id.length <6) errors.id="Employe ID should be 6 Numbers ";
        if(!this.state.data.id)errors.id="please Enter the ID";
        return errors;
    }
    render() {
       // const {errors}=this.state;
        return (

        <div className="FormCenter" >

         <form className="FormFields" 
         
         onSubmit={this.hnadlersubmit}  >

            <div className="FormField">
                <label className="FormField__Label" htmlFor="Id">Employe Id</label>

                <input 
                  className="FormField__Input" 
                  placeholder="Enter your ID" name="id"
                  value={this.state.data.id}
                  onChange={this.handleChange} />

                    <InlineError>{this.state.errors.id}</InlineError>
              </div>
             

              <div className="FormField">

                <label className="FormField__Label" htmlFor="password">Password</label>

                  <input 
                    type="password"
                    className="FormField__Input" 
                    placeholder="Enter your password" 
                    name="password"
                    value={this.state.data.password}
                    onChange={this.handleChange}/>

                     <InlineError>{this.state.errors.password}</InlineError>

              </div>

              <div className="FormField">
                  <button className="FormField__Button mr-20">Sign In</button> 
             
              </div>

            </form>
          </div>
        );
    }
}

export default FormLogin;