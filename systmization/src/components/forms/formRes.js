import './FormRes.css';
import './ResponsiveFormRes.css'
import React,{Component} from 'react';
import InlineError from '../messages/inlinError'
import {connect} from 'react-redux';
import {NotificationContainer, NotificationManager} from 'react-notifications';
var Loader = require('react-loader');   


var today= new Date();
var validator = require('validator');

class FormRes extends Component{
        
    
        state = {
           data:{ 
            id:'',
            date:this.getDatee(),
            day:this.dayhandler(),
            name:'',
            phone: '',
            respons: this.props.username,
            garsone:'',
            price:'',
            normalRes:'',
            res_date:'',
            res_day:'',
            res_time:'',
            res_number:'',
            formType:this.props.FormType
           },
          
            namClassName:'input_fields',
            phoneClassName: 'input_fields',
            responsClassName: 'input_fields',
            garsoneClassName:'input_fields',
            priceClassName:'input_fields',
            normalResClassName:'textarea',
            res_dateClassName:'input_fields',
            res_dayClassName:'input_fields',
            res_timeClassName:'input_fields',
            res_numberClassName:'input_fields',
           
           classType:"input-group",
            errors:{},
            loading:false,
            spinner:true,
            formClassname:'form_div col-m-9 col-s-12 col-s-12'
          
        };
 
getDatee(){

var month=(today.getMonth()+1)+'';

if (month.length<2){
    month='0'+month    
}
var dayDate=today.getDate()+'';
if(dayDate.length<2){
    dayDate='0'+dayDate;
}
return today.getFullYear()+'-'+ month+'-'+dayDate;
}
dayhandler(){
    var day=today.getDay();
    switch(day){
        case 0:
        return('الاحد');
        case 1:
        return('الاثنين');
        case 2:
        return('الثلاثاء');
        case 3:
        return('الاربعاء');
        case 4:
        return('الخميس');
        case 5:
        return('الجمعة')
        case 6:
        return('السبت');
        default:
        return ('');
    }
}


handleNameChange=(e)=>{
    this.setState({
        data: {...this.state.data, name:e.target.value},
    })

    if(!(/^[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]*$/).test(this.state.data.name)){
        this.setState({namClassName:"input_fields_error"}) 
        this.setState({errors:{...this.state.errors,name:" يرجى ادخال قيمة مكونة من الاحرف  "}})
    }
    else{ this.setState({namClassName:"input_fields"});
    this.setState({errors:{...this.state.errors,name:''}})
}
}

handlePhoneChange=(e)=>{ 
    this.setState({data:{...this.state.data,phone:e.target.value}});
    var phoneValue= this.state.data.phone;

    if(validator.isNumeric(phoneValue) && (/^(\d{6,16})$/).test(phoneValue)){
        this.setState({phoneClassName:"input_fields"})  
        this.setState({errors:{...this.state.errors,phone:""}})
    }    
    else{ this.setState({phoneClassName:"input_fields_error"})
    this.setState({errors:{...this.state.errors,phone:"يرجى ادخال قيمة مكونة من الارقام 7خانات او اكثر"}})
}
}

handleGarsoneChange=(e)=>{
    this.setState({data:{...this.state.data,garsone:e.target.value}});
    if((/^[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]*$/).test(this.state.data.garsone)){
        this.setState({garsoneClassName:"input_fields"}); 
        this.setState({errors:{...this.state.errors,garsone:''}})
    }
    else{this.setState({garsoneClassName:"input_fields_error"})
    this.setState({errors:{...this.state.errors,garsone:" يرجى ادخال قيمة مكونة من الاحرف  "}})
} 
}

handlePriceChange=(e)=>{
    this.setState({data:{...this.state.data,price:e.target.value}});
    if(validator.isFloat(this.state.data.price)){
        this.setState({priceClassName:"input_fields"});
        this.setState({errors:{...this.state.errors,price:''}})
 
    }
    else{this.setState({priceClassName:"input_fields_error"})
    this.setState({errors:{...this.state.errors,price:'يرجى ادخال قيمة مكونة من الارقام'}})

} 
}

handleNormalResChange=(e)=>{

    this.setState({data:{...this.state.data,normalRes:e.target.value}});
    if(this.state.data.normalRes!=="")
    {
        this.setState({normalResClassName:"textarea"});
        this.setState({errors:{...this.state.errors,normalRes:''}})
    }
}

handleRes_dateChange=(e)=>{
    this.setState({data:{...this.state.data,res_date:e.target.value}});
    if(this.state.data.res_date!=="")
    {
        this.setState({res_dateClassName:"input_fields"});
        this.setState({errors:{...this.state.errors,res_date:''}})
    }
}

handleres_dayChange=(e)=>{
    this.setState({
        data: {...this.state.data, res_day:e.target.value},
    },()=>{
        if(!this.state.data.res_day){        
            this.setState({errors:{...this.state.errors,res_day:" يرجى ادخال قيمة     "}})
            this.setState({res_dayClassName:"input_fields_error"});
            }
            else
            {this.setState({res_dayClassName:"input_fields"});
            this.setState({errors:{...this.state.errors,res_day:''}})     
        }
    })
   
}
handleres_timechange=(e)=>{
    this.setState({
        data: {...this.state.data, res_time:e.target.value},
    },()=>{
        if(!this.state.data.res_time){        
            this.setState({errors:{...this.state.errors,res_time:" يرجى ادخال قيمة     "}})
            this.setState({res_timeClassName:"input_fields_error"});
            }
            else
            {this.setState({res_timeClassName:"input_fields"});
            this.setState({errors:{...this.state.errors,res_time:''}})     
        }
    })
   
}
handleres_numberchange=(e)=>{
    this.setState({data:{...this.state.data,res_number:e.target.value}});
    if(validator.isNumeric(this.state.data.res_number)){
        this.setState({res_numberClassName:"input_fields"});
        this.setState({errors:{...this.state.errors,res_number:''}})
 
    }
    else{this.setState({res_numberClassName:"input_fields_error"})
    this.setState({errors:{...this.state.errors,res_number:'يرجى ادخال قيمة مكونة من الارقام'}})
}

}


handlersubmit=(e)=>{
if(this.props.isUpdate !== true){
e.preventDefault();
const errors=this.handlerValidator(this.state)
this.setState({ errors });
if(Object.keys(errors).length ===0 ){
    this.setState({spinner:false});
    this.props.submit(this.state.data)
    setTimeout(function(){
        this.setState({spinner:true});
    }.bind(this),500)
}
}
else{
    e.preventDefault();
    const errors=this.handlerValidator(this.state)
    this.setState({ errors });
    if(Object.keys(errors).length ===0 ){
        this.setState({spinner:false});
        //this.props.submit(this.state.data)
        this.props.updatedData(this.state.data);

        setTimeout(function(){
            this.setState({spinner:true});
        }.bind(this),500)
        //console.log(this.state.data)
    } 
}

setTimeout(function() { //Start the timer
    this.setState({spinner: true}) //After 1 second, set render to true
}.bind(this), 1000)
    
}

handlerValidator=(state)=>{
    const errors={};
    
    if(!this.state.data.name || !(/^[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_]*$/).test(this.state.data.name)){errors.name="يرجى ادخال قيمة مكونة من الاحرف";
    this.setState({namClassName:"input_fields_error"});
    }
    else
    {this.setState({namClassName:"input_fields"});
    }
    if(!this.state.data.phone ||!validator.isNumeric(this.state.data.phone) ||  (/^(\(?[0]{1}\)?\d{8,15})$/).test(this.state.data.phones))
    {errors.phone="يرجى ادخال قيمة مكونة من الارقام 7خانات او اكثر";
    this.setState({phoneClassName:'input_fields_error'});
    }
    else
    { this.setState({phoneClassName:"input_fields"});}

    if(!this.state.data.garsone || !(/^[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_]*$/).test(this.state.data.garsone)){errors.garsone="يرجى ادخال قيمة مكونة من الاحرف";
    this.setState({garsoneClassName:'input_fields_error'});}
    else
    { this.setState({garsoneClassName:"input_fields"});}

    if(!this.state.data.price || !validator.isFloat(this.state.data.price)) {errors.price="يرجى ادخال قيمة مكونة من الارقام";
    this.setState({priceClassName:'input_fields_error'});}
    else
    { this.setState({priceClassName:"input_fields"});}

    if(!this.state.data.res_date){errors.res_date="يرجى ادخال تاريخ الحجز في هذه الخانة ";
    this.setState({res_dateClassName:'input_fields_error'});}
    else
    {this.setState({res_dateClassName:"input_fields"});}

    if(!this.state.data.normalRes){errors.normalRes="يرجى ادخال قيمة في هذه الخانة ";
    this.setState({normalResClassName:'textarea_error'});
    errors.res_date=""
}
    else
    {this.setState({normalRes:"textarea"});}

    if(!this.state.data.res_day){errors.res_day="  يرجى ادخال قيمة في هذه الخانة";
        this.setState({res_dayClassName:"input_fields_error"});
        }
        else
        {this.setState({res_dayClassName:"input_fields"});     
    }

     if(!this.state.data.res_time){errors.res_time=" يرجى ادخال قيمة في هذه الخانة";
            this.setState({res_timeClassName:"input_fields_error"});
            }
            else
            {this.setState({res_timeClassName:"input_fields"});    
        }

        if(!this.state.data.res_number || !this.state.data.res_number){errors.res_number="يرجى ادخال قيمة مكونة من الارقام";
        this.setState({res_numberClassName:"input_fields_error"});
        }
        else
        {this.setState({res_numberClassName:"input_fields"});    
    }

    return errors;
}

componentDidMount(){
    if(this.props.isUpdate===true){
    this.setState({formClassname:'form_div_res col-m-12 col-s-12 col-s-12'})
    var newData=this.props.data;
    this.setState({
        data: {...this.state.data, id:newData.id, name:newData.name ,phone:newData.phone,respons:newData.resp_res
        ,garsone:newData.gar_res,garsone:newData.gar_res,price:newData.price ,
        normalRes:newData.res_normal ,res_date:newData.date_res ,res_day:newData.res_day ,
         res_time:newData.res_time , res_number:newData.res_number
        },
    })
    
    }
    
}
 
render(){

        return( 

  

    <div className='row'>
        <div className='main_div col-m-12 col-s-12 col-xs-12' >

        <form  onSubmit={this.handlersubmit}  className={this.state.formClassname} >

            <div className='title_div col-m-12 col-s-12 col-xs-12 '> <h1>حجز جديد </h1> </div> 
          

            <div className='form_sub_div col-m-12 col-s-12 col-xs-12'>  

                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    <div className='col-m-4 col-s-4 col-xs-4'> 
                        <label className='label_fields'> التاريخ</label></div>
                    <div className='col-m-8 col-s-8 col-xs-8'>
                
                    <label   name='date'
                             className='input_fields' 
                             value={this.state.date}
                             >{this.state.data.date}</label>           
                        
                        </div>
                        
                </div>

                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    <div className='col-m-4 col-s-4 col-xs-4'> 
                        <label className='label_fields'>اليوم  </label></div>
                    <div className='col-m-8 col-s-8 col-xs-8'>
                        <label className='input_fields' value={this.state.day} name='day'>{this.state.data.day}</label>                        
                        </div>

                    
                </div> 

            </div>
    
            <div className='form_sub_div col-m-12 col-s-12 col-xs-12'>  

                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    <div className='col-m-4 col-s-4 col-xs-4'> 
                        <label className='label_fields'> الاسم</label></div>
                    <div className='col-m-8 col-s-8 col-xs-8'>
                  
                    <input  className={this.state.namClassName}
                         value={this.state.data.name} 
                         name='name'
                         onChange={this.handleNameChange}
                         
                        ></input>            
                           <InlineError>{this.state.errors.name}</InlineError>
                          
                          </div>
                        
                </div>

                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    <div className='col-m-4 col-s-4 col-xs-4'> 
                        <label className='label_fields'>رقم الهاتف </label></div>
                    <div className='col-m-8 col-s-8 col-xs-8'>
                        <input  className={this.state.phoneClassName}
                         value={this.state.data.phone} 
                         name='phone'
                         onChange={this.handlePhoneChange}
                         maxLength='16'

                         ></input>
                         <InlineError>{this.state.errors.phone}</InlineError> 
                        </div>

                       
                </div> 

            </div>
         
            <div className='form_sub_div col-m-12 col-s-12 col-xs-12'> 

                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    <div className='col-m-4 col-s-4 col-xs-4'>
                        <label className='label_fields'> مسؤول الحجز</label></div>
                    
                        <div className='col-m-8 col-s-8 col-xs-8'>
                	
                    <label  className={this.state.responsClassName}
                        >{this.state.data.respons}</label>
                           <InlineError>{this.state.errors.respons}</InlineError>
                        </div>
                        
                </div>        

                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    <div className='col-m-4 col-s-4 col-xs-4'>
                        <label className='label_fields'>  السفرجي المسؤول </label></div>
                    
                    <div className='col-m-8 col-s-8 col-xs-8'>
                        <input  className={this.state.garsoneClassName}
                         value={this.state.data.garsone} 
                         name='garsone'
                         onChange={this.handleGarsoneChange}
                        ></input> 
                        <InlineError>{this.state.errors.garsone}</InlineError>
                        </div>

                </div>
            </div>    
      

          
            <div className='form_sub_div col-m-12 col-s-12 col-xs-12'> 
                
                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    <div className='col-m-4 col-s-4 col-xs-4'>
                        <label className='label_fields'> اليوم </label></div>
                    
                    <div className='col-m-8 col-s-8 col-xs-8'>
                        <select  className={this.state.res_dayClassName}
                         value={this.state.data.res_day} 
                         name='res_day'
                         onChange={this.handleres_dayChange}
                        >
                        <option value='' selected disabled hidden>اختر اليوم</option>
                        <option value='السبت'>السبت</option>
                        <option value='الاحد'>الاحد</option>
                        <option value='الاثنين'>الاثنين</option>
                        <option value='الثلاثاء'>الثلاثاء</option>
                        <option value='الاربعاء'>الاربعاء</option>
                        <option value='الخميس'>الخميس</option>
                        <option value='الجمعة'>الجمعة</option>

                        </select> 
                        <InlineError>{this.state.errors.res_day}</InlineError> 
                    </div>

                </div>        

                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    <div className='col-m-4 col-s-4 col-xs-4'>
                        <label className='label_fields'>  تاريخ الحجز </label></div>
                    
                    <div className='col-m-8 col-s-8 col-xs-8'>
                        <input  className={this.state.res_dateClassName}
                         value={this.state.data.res_date} 
                         name='res_date'
                         onChange={this.handleRes_dateChange}
                         type='date'
                        ></input> 
                        <InlineError>{this.state.errors.res_date}</InlineError>
                        </div>
                </div>
            </div>   


            
            <div className='form_sub_div col-m-12 col-s-12 col-xs-12'> 
                
                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    <div className='col-m-4 col-s-4 col-xs-4'>
                        <label className='label_fields'> الساعة </label></div>
                    
                    <div className='col-m-8 col-s-8 col-xs-8'>

                     <input   className={this.state.res_timeClassName}
                         value={this.state.data.res_time} 
                         name='res_time'
                         onChange={this.handleres_timechange}
                         
                        ></input> 
                        <InlineError>{this.state.errors.res_time}</InlineError> 
                    </div>

                </div>        

                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    <div className='col-m-4 col-s-4 col-xs-4'>
                        <label className='label_fields'> عدد الاشخاص </label></div>
                    
                    <div className='col-m-8 col-s-8 col-xs-8'>
                        <input  className={this.state.res_numberClassName}
                         value={this.state.data.res_number} 
                         name='res_number'
                         onChange={this.handleres_numberchange}
                        ></input> 
                        <InlineError>{this.state.errors.res_number}</InlineError>
                        </div>

                </div>
            </div> 

            <div className='form_sub_div col-m-12 col-s-12 col-xs-12'> 

                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    <div className='col-m-4 col-s-4 col-xs-4'>
                        <label className='label_fields'> السعر </label></div>
                    
                        <div className='col-m-8 col-s-8 col-xs-8'>
                	
                        <input  className={this.state.priceClassName}
                         value={this.state.data.price} 
                         name='price'
                         onChange={this.handlePriceChange}

                        ></input>
                           <InlineError>{this.state.errors.price}</InlineError>
                     </div>
                        
                </div>

                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    <div className='col-m-4 col-s-4 col-xs-4'>        
                        <label className='label_fields'>طبيعة الحجز</label></div>
                    <div className='col-m-4 col-s-8 col-xs-8'>

                        <textarea type='text' className={this.state.normalResClassName} 
                         value={this.state.data.normalRes} 
                         name='normalRes'
                         onChange={this.handleNormalResChange}
                        ></textarea>
                        <InlineError>{this.state.errors.normalRes}</InlineError>

                        </div>

                </div> 
            </div> 
      


      
            <div className='form_sub_div col-m-12 col-s-12 col-xs-12'> 
               
                {this.props.isUpdate ===true &&
                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    
                <div className='button_div col-m-12 col-s-12 col-xs-12  '> 
                    {this.state.spinner ?<button className='button_style'>تعديل الحجز</button> :
                <Loader top='auto' left='auto'></Loader>

                }
                 {this.state.spinner=== false &&  NotificationManager.success( 'تم تعديل الحجز بنجاح','تعديل الحجز' )}
            <NotificationContainer/>

                </div>    

            </div>
                }
          </div>  
       

        {this.props.isUpdate !==true &&
        <div className='row'>
      

            <div className='button_div col-m-12 col-s-12 col-xs-12  '> 
                    {this.state.spinner ?<button className='button_style'>ادخال الحجز </button> :
                <Loader top='auto' left='auto'></Loader>
                }
                  {this.state.spinner=== false &&  NotificationManager.success( 'تم اضافة حجز بنجاح',' حجز جديد' )}
            <NotificationContainer/>
                
                
            </div>  
          
        </div>        
        }
           
        
 

    </form>
        
        
        </div>

        </div>
        
        );
    }
}

const mapdispatch = dispatch =>{
    return {    
    }
}

const maptToProps=(state)=>{
    return{
        username:state.auth.username 
    }
  }
export default connect(maptToProps,mapdispatch)(FormRes)
