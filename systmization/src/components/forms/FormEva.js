import './FormRes.css';
import React,{Component} from 'react';
import NewRow from './newRow'
import NewRow2 from './newRow2'
import InlineError from '../messages/inlinError'
import {connect} from 'react-redux';
import Zuwar from '../images/zuwar.jpg';
import BIM from '../images/BIM.jpg';
import ORCAS from '../images/ORCAS-Small.png';
import Futur from '../images/future.jpg';

var Loader = require('react-loader');   
var today=new Date();

class FormEva extends Component{
    state={
       data:{
           shiftResp:this.props.username,
           shift:'',
           date:today.getFullYear()+'-'+ (today.getMonth()+1)+'-'+today.getDate(),
           day:this.dayhandler(),
           formType:this.FormType(),
           emergencyState:'',
           notes:'',
           achievement:'',         
       } ,
       employeestate:[
        {name:'',reason:''}
    ],
    employeeLate:[{name:'',reason:''}],
    employeeLeft:[{name:'',reason:''}],
    errors:{},

    shiftRespClassName:'input_fields',
    shiftClassName:'input_fields',

    spinner:true,
    image:'',

    }

FormType(){
    var d;

    if( this.props.FormType=="zuwarEnter" ||  this.props.FormType=="EnterViewDailyReportZuwer"){
        d="zuwarEnter"
     
    }
    else if( this.props.FormType=="orcasEnter"||  this.props.FormType=="addOrcasReport"){
        d="orcasEnter"
        
    }
    else if ( this.props.FormType=="FutureEnter" ||  this.props.FormType=="AddFutureReport"){
        d="FutureEnter"
      
    }
    else if ( this.props.FormType=="BIMEnter" ||  this.props.FormType=="addBIMReport"){
        d="BIMEnter"
      
    }

    //this.setState([{data:{...this.state.data,formType:d}}])
    return d;
}


componentDidMount(){    
    if( this.props.FormType=="zuwarEnter" ||  this.props.FormType=="EnterViewDailyReportZuwer"){
        this.setState({image:Zuwar});
    }
    else if( this.props.FormType=="orcasEnter"||  this.props.FormType=="addOrcasReport"){
        this.setState({image:ORCAS});
    }
    else if ( this.props.FormType=="FutureEnter" ||  this.props.FormType=="AddFutureReport"){
        this.setState({image:Futur});
    }
    else if ( this.props.FormType=="BIMEnter" ||  this.props.FormType=="addBIMReport"){
        this.setState({image:BIM});
    }

}
handleChange=(e)=>{
    console.log(e.target.className)
    if(["name","reason"].includes(e.target.className)){
        let employeestate=[...this.state.employeestate]
        employeestate[e.target.dataset.id][e.target.className]=e.target.value
        this.setState({employeestate},()=>{console.log(this.state.employeestate)})
        console.log("goodbuy")
    }else{
    this.setState({[e.target.name]:e.target.value});
    console.log('welcome');
    }
}

handleEmployeeLateChange=(e)=>{
    console.log(e.target.className)
    if(["name","reason"].includes(e.target.className)){
        let employeeLate=[...this.state.employeeLate]
        employeeLate[e.target.dataset.id][e.target.className]=e.target.value
        this.setState({employeeLate},()=>{console.log(this.state.employeeLate)})
        console.log("goodbuy")
    }else{
    this.setState({[e.target.name]:e.target.value});
    console.log('welcome');
    }
}

handleEmployeeLeftChange=(e)=>{

    if(["name","reason","time"].includes(e.target.className)){
        let employeeLeft=[...this.state.employeeLeft]
        employeeLeft[e.target.dataset.id][e.target.className]=e.target.value
        this.setState({employeeLeft},()=>{console.log(this.state.employeeLeft)})
        console.log("goodbuy")
    }else{
    this.setState({[e.target.name]:e.target.value});
    console.log('welcome');
    }
}

handerlAddEmployee = (e) => {
    e.preventDefault();
    this.setState((prevState) => ({
        employeestate: [...prevState.employeestate, {name:"", reason:""}],
    }));
   
}

handerlAddEmployeeLate = (e) => {
    e.preventDefault();
    this.setState((prevState) => ({
        employeeLate: [...prevState.employeeLate, {name:"", reason:""}],
    }));
} 
    
handerlAddEmployeeLeft= (e) => {
    e.preventDefault();
    this.setState((prevState) => ({
        employeeLeft: [...prevState.employeeLeft, {name:"", reason:""}],
    }));
} 

handleSubmit=(e)=>{
    e.preventDefault();
    const errors=this.handlerValidator(this.state)
    this.setState({ errors });

    if(Object.keys(errors).length ===0 ){
        this.props.submit(this.state);
        this.setState({spinner:false});
    }else{
        alert(Object.keys(errors).length)
    }

    setTimeout(function() { //Start the timer
        this.setState({spinner: true}) //After 1 second, set render to true
    }.bind(this), 1000)
}

handlerValidator=(state)=>{
    const errors={};


    if(!this.state.data.shift){errors.shift="يرجى ادخال قيمة   ";
    this.setState({shiftClassName:"input_fields_error"});
    }
    else
    {this.setState({shiftClassName:"input_fields"});}

    return errors;
}
    
dayhandler(){
    var day=today.getDay();
    switch(day){
        case 0:
        return('الاحد');
        case 1:
        return('الاثنين');
        case 2:
        return('الثلاثاء');
        case 3:
        return('الاربعاء');
        case 4:
        return('الخميس');
        case 5:
        return('الجمعة')
        case 6:
        return('السبت');
        default:
        return ('');
    }
}

handleShiftChange=(e)=>{
    this.setState({
        data: {...this.state.data, shift:e.target.value},
    },()=>{
        if(!this.state.data.shift){        
            this.setState({errors:{...this.state.errors,shift:" يرجى ادخال قيمة     "}})
            this.setState({shiftClassName:"input_fields_error"});
            }
            else
            {this.setState({shiftClassName:"input_fields"});
            this.setState({errors:{...this.state.errors,shift:''}})
        
        }
    })
}
handlenotesChange=(e)=>{
    this.setState({data:{...this.state.data , notes:e.target.value}})
}
handleEmergencyChange=(e)=>{
    this.setState({data:{...this.state.data , emergencyState:e.target.value}})
}
handleAchevChange=(e)=>{
    this.setState({data:{...this.state.data , achievement:e.target.value}})
}


    render(){
      let {employeestate}=this.state
      let {employeeLate}=this.state
      let {employeeLeft}=this.state
        return( 
            
    <div className='row'>
    <div className='main_div col-m-12 col-s-12 col-xs-12' >

       <form className='form_div col-m-9 col-s-12 col-s-12'  onSubmit={this.handleSubmit} >
       <div className='title_div col-m-12 col-s-12 col-xs-12 '> <img src={this.state.image} style={{width:150, height:150  }}/> </div>   
        <div className='title_div col-m-12 col-s-12 col-xs-12 '> <h1>تقييم جديد </h1> </div> 
      

    <div className='form_sub_div col-m-12 col-s-12 col-xs-12'>  

        <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                <div className='col-m-4 col-s-4 col-xs-4'> 
                        <label className='label_fields '>مسؤول الوردية</label> </div> 

                <div className='col-m-8 col-s-8 col-xs-8'>
                  
                  <label  className={this.state.shiftRespClassName}
                      
                      >{this.state.data.shiftResp} </label>            
                        <InlineError>{this.state.errors.shiftResp}</InlineError>
                        </div>
                     
        
        </div>

    
        <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                <div className='col-m-4 col-s-4 col-xs-4'>
                    <label className='label_fields' >الوردية</label> </div>
                
                <div className='col-m-8 col-s-8 col-xs-8'>

                   <div className='col-m-8 col-s-8 col-xs-8'>
                  
                  <select  className={this.state.shiftClassName}
                       value={this.state.data.shift} 
                       name='shift'
                       onChange={this.handleShiftChange.bind(this)}
                      
                      >
                        <option value='' selected disabled hidden>اختر الوردية</option>
                        <option value='A'>A</option>
                        <option value='B'>B</option>
                        <option value='C'>C</option>
                      
                      </select>            
                        <InlineError>{this.state.errors.shift}</InlineError>
                        </div>

                 
                </div>
                    
        </div>


             <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                <div className='col-m-4 col-s-4 col-xs-4'> 
                        <label className='label_fields '> اليوم</label> </div> 

                   <div className='col-m-8 col-s-8 col-xs-8'>
                        <label className='input_fields'> {this.state.data.day}</label> </div>
           
           </div>
           <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                <div className='col-m-4 col-s-4 col-xs-4'> 
                        <label className='label_fields '>التاريخ </label> </div> 

                    <div className='col-m-8  col-s-4 col-xs-4'>
                        <label className='input_fields '>{this.state.data.date}</label> </div>  
           </div>

        </div>
  
       
   
        <div className='form_sub_div col-m-12 col-s-12 col-xs-12'>  
            <div className='flex_start col-m-12 col-xs-12 col-s-12'>
                <label className='label_fields'><strong>الموظفين الغائبين</strong></label>
            </div>        
       
            <div className='flex_start col-m-12 col-xs-12 col-s-12'>
                <button className='button_fields ' onClick={this.handerlAddEmployee}>+</button> </div> 
                   
            </div>
                   
                <form onChange={this.handleChange}>
                    <NewRow name={employeestate}></NewRow>
                </form>
       
     
        <div className='form_sub_div col-m-12 col-s-12 col-xs-12'>  
            <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                <label className='label_fields'><strong>الموظفين المتاخرين عن عملهم</strong></label></div>
        

            <div className='flex_start col-m-12 col-xs-12 col-s-12'>
                <button className='button_fields ' onClick={this.handerlAddEmployeeLate}>+</button> </div> 
            </div>
                    <form onChange={this.handleEmployeeLateChange}>
                        <NewRow name={employeeLate}></NewRow>
                    </form>
                  


    
        <div className='form_sub_div col-m-12 col-s-12 col-xs-12'>  
            <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                <label className='label_fields'><strong>اذن المغادرة</strong></label></div>
           
            <div className='flex_start col-m-12 col-xs-12 col-s-12'>
                    <button className='button_fields ' onClick={this.handerlAddEmployeeLeft}>+</button> </div> 
            </div>
                   
                    <form onChange={this.handleEmployeeLeftChange}>
                        <NewRow2  
                        name={employeeLeft}></NewRow2>
                    </form>
       
    

    <div className='row'>
        <div className='form_sub_div col-m-12 col-s-12 col-xs-12'> 
            <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                <div className='col-m-4 col-s-4 col-xs-4'>        
                    <label className='label_fields'>الحالات الطارئة </label></div>
                <div className='col-m-4 col-s-8 col-xs-8'>
                    <textarea type='text' className='textarea' 
                    onChange={this.handleEmergencyChange}
                    name='emergencyState'
                    value={this.state.data.emergencyState}
                    ></textarea></div>
            </div>

             <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                <div className='col-m-4 col-s-4 col-xs-4'>        
                    <label className='label_fields'> الملاحظات </label></div>
                <div className='col-m-4 col-s-8 col-xs-8'>
                    <textarea type='text' className='textarea'
                    onChange={this.handlenotesChange}
                    name='notes'
                    value={this.state.data.notes}
                    ></textarea></div>
            </div>    
          
        </div>
    </div>



    <div className='row'>
        <div className='button_div col-m-12 col-s-12 col-xs-12 '> 
            {this.state.spinner ?<button className='button_style'>ادخال التقييم </button> :
                <Loader top='auto' left='auto'></Loader>
                }
                
        </div>          
    </div>        
           
       
    

     </form>
    
    </div>

    </div>
        
        );
    }
}


const mapdispatch = dispatch =>{
    return {
    }
}
const maptToProps=(state)=>{
    return{
       username:state.auth.username
    }
  }


export default connect(maptToProps,mapdispatch)(FormEva)
