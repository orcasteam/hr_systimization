
import './FormRes.css';
import React,{Component} from 'react';
import InlineError from '../messages/inlinError'
import FormEva_edit from '../forms/formEva_edit'
import FormEvaDisplay from '../forms/formEvaDisplay'


class ChooseShift extends Component{
    state={
        formType:this.props.FormType,
        shift:'',
        shiftClassName:'input_fields',
        errors:{},
        value:false,
    }
    handleShiftChange=(e)=>{
        e.preventDefault();
        this.setState({shift:e.target.value});
        this.setState({shiftClassName:"input_fields"});
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        const errors=this.handlerValidator(this.state)
        this.setState({ errors });
        if(Object.keys(errors).length ===0 ){
           this.setState({value:true})
        }

    }
    handlerValidator=(state)=>{
        const errors={};
        if(!this.state.shift){errors.shift="يرجى ادخال قيمة   ";
        this.setState({shiftClassName:"input_fields_error"});
        }
        else
        {this.setState({shiftClassName:"input_fields"});}

        return errors;

    }
    render(){
        return(

        <div>
            {this.state.value===false &&
           <div className='popup'>
        <div className='popup_inner'>
        <form className=' col-m-12 col-s-12 col-s-12'  onSubmit={this.handleSubmit} >
            <div className='title_div col-m-12 col-s-12 col-xs-12 '> <h1>يجب عليك اختيار الوردية </h1> </div> 
        
            <div className='flex_start col-m-12 col-xs-12 col-s-12'>
                    <div className='col-m-4 col-s-4 col-xs-4'>
                        <label className='label_fields' >الوردية</label> </div>
                    
                    <div className='col-m-8 col-s-8 col-xs-8'>

                    <div className='col-m-8 col-s-8 col-xs-8'>
                    
                    <select  
                        className={this.state.shiftClassName}
                         value={this.state.shift} 
                        name='shift'
                        onInput={this.handleShiftChange.bind(this)}
                        >
                            <option value='' selected disabled hidden>اختر الوردية</option>
                            <option value='A'>A</option>
                            <option value='B'>B</option>
                            <option value='C'>C</option>
                        
                        </select>            
                        <InlineError>{this.state.errors.shift}</InlineError>
                            </div>

                    
                    </div>
                        
            </div>

        <div className='button_div col-m-12 col-s-12 col-xs-12 '> 
            <button className='button_style_popup'>تم</button> 
                
        </div>          
    

            </form>
      
        </div>
      </div>
            }
        {(this.state.value===true && this.props.isDisplay!==true) && 
         <FormEva_edit FormType={this.state.formType} shift={this.state.shift}></FormEva_edit>   
    }
    {(this.state.value===true && this.props.isDisplay===true) &&
     <FormEvaDisplay FormType={this.state.formType} shift={this.state.shift}></FormEvaDisplay>    
    }

      </div>
        )
    }
}

export default ChooseShift