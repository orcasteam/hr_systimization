

import './FormRes.css';
import React,{Component} from 'react';
import QualityDisplay from './QualityDisplay'
import {connect} from 'react-redux';
import Zuwar from '../images/zuwar.jpg'
import {updateQuality} from "../actions/auth";

var jsPDF =require('jspdf')
var html2canvas=require('html2canvas')
var Loader = require('react-loader');   


class formEvaDisplayAdmin extends Component{
    state={
        isAdmin:true,
        quality:'',
        qualityClassName:'textarea_quality',
        qualityerror:'',
    }
    handleDisplay(x){
        this.props.deactive()
    }
    componentDidMount(){
        if(this.props.isedit===true){
            this.setState({quality:this.props.data.note});
        }
    }
    handlerQualityChange=(e)=>{
        this.setState({quality:e.target.value})
        console.log(this.state.quality)

        if(this.state.quality!=="")
    {
        this.setState({qualityClassName:'textarea_quality'});
        this.setState({qualityerror:''})
    }
    }
    //----------------------
    handleSubmit=(e)=>{
        e.preventDefault();
        if(this.state.quality ==""){
            this.setState({qualityerror:true});
            this.setState({qualityClassName:'textarea_quality_error'})
        }
        else{
            var obj={note:this.state.quality ,id:this.props.data.id};
            //this.props.qualityInsert(obj)
            this.props.updateQuality(obj);
           /* setTimeout(function(){
               
            }.bind(this),500)*/
            
            this.props.deactive({obj});
        }
    }
    // -----------------------
    render(){
        return(
    <div className='popup'>
            <button  className="closeButton" onClick={()=>this.handleDisplay("welcome")}>x</button>

            <div className='popup-inner_display'>
               
            {this.props.isedit !==true ?
                    <QualityDisplay isAdmin={this.state.isAdmin} data={this.props.data}></QualityDisplay> :

                    <form className='form_div_edit' onSubmit={this.handleSubmit}>
                        <div className='title_div col-m-12 col-s-12 col-xs-12 '> <img src={Zuwar} style={{width:120, height:110}}/> </div>   

                        <div className='title_div col-m-12 col-s-12 col-xs-12 '> <h2>تقرير الجودة</h2> </div>
                        
                        <div className='form_sub_div col-m-12 col-s-12 col-xs-12'>  

                            <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                                <div className='col-m-4 col-s-4 col-xs-4'> 
                                    <label className='label_fields'> المسؤول</label></div>
                                <div className='col-m-8 col-s-8 col-xs-8'>
                            
                                <label  className='input_fields'>{this.props.data.name}</label>            
                                    </div>
                                    
                            </div>

                            <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                                <div className='col-m-4 col-s-4 col-xs-4'> 
                                    <label className='label_fields'>التاريخ</label></div>
                                <div className='col-m-8 col-s-8 col-xs-8'>
                                <label  className='input_fields'>{this.props.data.date}</label>            

                                    
                                    </div>

                                
                            </div> 

                        </div>

                   
                        <div className='form_sub_div_quality col-m-12 col-s-12 col-xs-12'> 
                        <label className="label_quality"><h4> ملاحظات عامة عن المطعم من إدارة المطعم</h4></label>

                        <textarea cols="50" rows="10" className={this.state.qualityClassName}
                        name='quality'
                        value={this.state.quality}
                        onChange={this.handlerQualityChange}
                        onInput={this.handlerQualityChange}
                        ></textarea>
                        </div>

                        <div className='button_div_qualiy col-m-12 col-s-12 col-xs-12  '> 
                        

                            <button className='button_style'>حفظ التقرير</button>
                           
                        
                        </div>

            
                    </form>
                }
            </div>
    
    </div>
  

        )}

}

const mapdispatch = dispatch =>{
    return {
        updateQuality:(data)=>dispatch(updateQuality(data)),
    }
}
const maptToProps=(state)=>{
    return{
     
    }
  }

export default connect(maptToProps,mapdispatch)(formEvaDisplayAdmin)