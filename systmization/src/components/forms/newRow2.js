
import './FormRes.css';
import React,{Component} from 'react';

const newRow = (props) =>{
    return(             
            props.name.map((val,idx)=>{
             
                let employeeId=`employee-${idx}`
                let resonId=`res-${idx}`
                let timeId=`time-${idx}`

                return(


                    <div className='row' key={idx}>
                     <div className='flex_start col-m-4 col-xs-12 col-s-12'>
                    <div className='col-m-4 col-s-4 col-xs-4'>
                        <label
                         className='label_fields'
                         htmlFor={employeeId}
                          >{`الاسم # ${idx+1}`} </label> </div>  
                
                    <div className='input_container col-m-6 col-s-8 col-xs-5'> 
                         <input 
                         type="text"
                          className='name'
                          name={employeeId}
                          data-id={idx}
                          id={employeeId}
                          value={val.name}
                       
                          ></input> </div>         
                </div>  

                  <div className='flex_start col-m-4 col-xs-12 col-s-12'>
                    <div className='col-m-3 col-s-4 col-xs-4'> 

                             <label className='label_fields'>   السبب </label></div>

                    <div className='input_container col-m-6 col-s-8 col-xs-5'> 
                            <input 
                            type="text"
                             className='reason'
                             name={resonId}
                             data-id={idx}
                             id={resonId}
                             value={val.reason}
                            ></input></div>
                    </div>

                    
                    <div className='flex_start col-m-4 col-xs-12 col-s-12'>
                    <div className='col-m-6 col-s-4 col-xs-4'> 

                             <label className='label_fields'> ساعة المغادرة </label></div>

                    <div className='input_container col-m-5 col-s-8 col-xs-5'> 
                            <input 
                            type="text"
                             className='time'
                             name={timeId}
                             data-id={idx}
                             id={timeId}
                             value={val.time}
                            ></input></div>
                   

                    </div>
                    
                    </div>       
                   
                  
                

                )


            })
          
        
    )
}


export default newRow