import React,{Component} from 'react';
import InlineError from '../messages/inlinError'
import './FormRes.css';
import './ResponsiveFormRes.css'
import {connect} from "react-redux"
import {getIdUser,EditUser} from "../actions/auth"
var Loader = require('react-loader');   
var validator = require('validator');

const WAIT_INTERVAL = 1000
const ENTER_KEY = 13

class createNewUser extends Component{

    
        state={
            data:{
                idUserEdit:'',
                empName:'',
                empNumber:'',
                empPassword:'',
                empRepPassword:'',
                department:''
            },
            errors:{},
            empNamClassName:'input_fields',
            empNumberClassName:'input_fields',
            empPasswordClassName:'input_fields',
            empRepPasswordClassName:'input_fields',
            empDepartmentClassName:'input_fields',
            spinner:true,
            name: '',
            value:'',
         
          
        }
        timer = null
    
    
          handleKeyDown = e => {
            
            if (e.keyCode === ENTER_KEY) {
              clearTimeout(this.timer)
              this.triggerChange()
            }
            
          }
          triggerChange = () => {
            const { value } = this.state
            //console.log("wait here")
           // this.props.onChange(value)
           console.log("stop we are here")
          

           this.props.getId(this.state.data.empNumber)

           setTimeout(function() { //Start the timer
            console.log(this.props.userFoundResult+"mohammad") //After 1 second, set render to true
            if(this.state.data.empNumber>5 && this.props.userFoundResult ==false){

               this.setState({errors:{...this.state.errors,empNumber:"هذا الرقم محجوز الرجاء ادخال رقم اخر"}})
                this.setState({empNumberClassName:"input_fields_error"}) 

            }
        }.bind(this), 500)
           
          }

    handlersubmit=(e)=>{
        e.preventDefault();
        const errors=this.handlerValidator(this.state)
        this.setState({ errors });
        
        if(Object.keys(errors).length ===0 ){
            this.setState({spinner:false});
            this.props.createNewUser(this.state.data)
           
         //  alert("welcome")
        }else{
            alert(Object.keys(errors).length)
        }
        
        
        setTimeout(function() { //Start the timer
            this.setState({spinner: true}) //After 1 second, set render to true
        }.bind(this), 1000)
            
        }


    handlerValidator=(state)=>{
        const errors={};

        if(!this.state.data.empName || 
           !(/^[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]*$/)
          .test(this.state.data.empName)){

            errors.empName="يرجى ادخال قيمة مكونة من الاحرف";
            this.setState({empNamClassName:"input_fields_error"});

                  }

        if(!this.state.data.empNumber ||!validator.isNumeric(this.state.data.empNumber) ||  !(/^(\d{5,16})$/).test(this.state.data.empNumber))
        {errors.empNumber="يرجى ادخال قيمة مكونة من الارقام 5خانات او اكثر";
        this.setState({empNumberClassName:'input_fields_error'});
             }

             if(!this.state.data.empPassword || !(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/).test(this.state.data.empPassword)){

                this.setState({empPasswordClassName:"input_fields_error"})
                errors.empPassword="يرجى ادخال قيمة مكونة من 6 خانات او اكثر تحتوي على حروف وارقام ورموز";
                
             }

             if(this.state.data.empRepPassword !== this.state.data.empPassword || !this.state.data.empRepPassword ){
                this.setState({empRepPasswordClassName:"input_fields_error"})
                errors.empRepPassword="كلمة السر غير متطابقة";
               
            }else{
                this.setState({empRepPasswordClassName:" input_fields_success"})
              
            }
            if(!this.state.data.department){
          
                this.setState({empDepartmentClassName:"input_fields_error"})
               
                    errors.empDepartment="ارجاء احدى الاقسام"
                }
                if(this.state.data.empNumber>5 && this.props.userFoundResult ==false){

                    errors.empNumber="هذا الرقم محجوز الرجاء ادخال رقم اخر  "
                     this.setState({empNumberClassName:"input_fields_error"}) 
     
                 }


    return errors;

    
    }

    /*--------------------------HandlechangeInput---------------------*/

    handlerEmpName=(e)=>{
    
        this.setState({
            data: {...this.state.data, empName:e.target.value},
        },()=>{
            if(!(/^[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]*$/).test(this.state.data.empName)){
                this.setState({empNamClassName:"input_fields_error"}) 
                this.setState({errors:{...this.state.errors,empName:" يرجى ادخال قيمة مكونة من الاحرف  "}})
            }
            else{ this.setState({empNamClassName:"input_fields_success"});
            this.setState({errors:{...this.state.errors,empName:''}})
        }
       
        })
    
  
    }

    handlerempNumber=(e)=>{
        clearTimeout(this.timer)
        this.setState({
            data:{...this.state.data,empNumber:e.target.value},
        },()=>{
            var empNumber= this.state.data.empNumber;

            if(validator.isNumeric(empNumber) && (/^(\d{5,16})$/).test(empNumber)){
                this.setState({empNumberClassName:"input_fields_success"})  
                this.setState({errors:{...this.state.errors,empNumber:""}})
            }    
            else{
                 this.setState({empNumberClassName:"input_fields_error"})
                this.setState({errors:{...this.state.errors,empNumber:"يرجى ادخال قيمة مكونة من الارقام 5 خانات او اكثر"
            }})
     
                      }
       
    
   
        }) 
        this.timer = setTimeout(this.triggerChange, WAIT_INTERVAL)

      
    
    }


    handlerEmpPassword=(e)=>{
        this.setState({
            data:{...this.state.data,empPassword:e.target.value}
        },()=>{

            if((/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/).test(this.state.data.empPassword)){

                this.setState({empPasswordClassName:"input_fields_success"})  
                this.setState({errors:{...this.state.errors,empPassword:""}})
                
            }else{
                this.setState({empPasswordClassName:"input_fields_error"})
                this.setState({errors:{...this.state.errors,
                    empPassword:"يرجى ادخال قيمة مكونة من 6 خانات او اكثر تحتوي على حروف وارقام ورموز"
                }})
    
            }
            
        })

           
       
    }
    
    hnadlerRepPassowrd=(e)=>{
        this.setState({
            data:{...this.state.data,empRepPassword:e.target.value},
        }, () => {
           
        if(this.state.data.empRepPassword !== this.state.data.empPassword || !this.state.data.empRepPassword){
            this.setState({empRepPasswordClassName:"input_fields_error"})
            this.setState({
                errors:{...this.state.errors,empRepPassword:"كلمة السر غير متطابقة"}
            })
         
        }else{
            this.setState({empRepPasswordClassName:"input_fields_success"})
            this.setState({
                errors:{...this.state.errors,empRepPassword:""}
            })

        }
          })
 
//console.log(this.state.data.empPassword)
//  console.log(this.state.data.empRepPassword)
        
    }
handlerDepartment=(e)=>{
    this.setState({
        data:{...this.state.data,department:e.target.value}
    },()=>{
        if(!this.state.data.department){
          
            this.setState({empDepartmentClassName:"input_fields_error"})
            this.setState({
                errors:{...this.state.errors,empDepartment:"ارجاء احدى الاقسام"}})
            }
        else
        {
            this.setState({empDepartmentClassName:"input_fields_success"})
            this.setState({
                errors:{...this.state.errors,empDepartment:""}
            })
            }
    })
//console.log(e.target.value)
//console.log(this.state.data.department)

}

/*------------------------handlerEditPrps-------------------------*/
componentWillReceiveProps(nextProps){
    console.log(nextProps.userEditType)
    this.handlerprops(nextProps.userEditType)
}

handlerprops=(dataProp)=>{

if(dataProp){
    this.setState({
        data:{...this.state.data,
            empNumber:dataProp.userId,
            empName:dataProp.userName,
            empPassword:dataProp.password,
            empRepPassword:dataProp.password,
            department:dataProp.types,
            idUserEdit:dataProp.id

        },
        errors:{},
            empNamClassName:'input_fields',
            empNumberClassName:'input_fields',
            empPasswordClassName:'input_fields',
            empRepPasswordClassName:'input_fields',
            empDepartmentClassName:'input_fields',

    })
}
}

handlerSubmitEdit=(e)=>{
  

    e.preventDefault();
        const errors=this.handlerValidator(this.state)
        this.setState({ errors });
        
        if(Object.keys(errors).length ===0 ){
            this.setState({spinner:false});
          this.props.EditUser(this.state.data)
         //  alert("welcome")
         //window.location.reload(); 
         this.props.editUserRes(this.state.data)
        
        }else{
            alert(Object.keys(errors).length)
        }
        
        
        setTimeout(function() { //Start the timer
            this.setState({spinner: true}) //After 1 second, set render to true
        }.bind(this), 1000)
            

}



   render(){
    //  console.log(this.props.userEditType)
    //{this.props.userEditType&& this.hnadlerEdit(this.props.userEditType)}

   
        return(

         <div className='row'>


         <div className='main_div col-m-12 col-s-12 col-xs-12' >


          
         <form className={this.props.userEditType?'for_div_edit col-m-9 col-s-12 col-s-12':'form_div col-m-9 col-s-12 col-s-12'}  onSubmit={this.props.userEditType?this.handlerSubmitEdit:this.handlersubmit}>

        <div className='title_div col-m-12 col-s-12 col-xs-12 '>{this.props.userEditType?<h1> تعديل مستخدم</h1>:<h1>انشاء مستخدم جديد </h1>}  </div> 
          

                 
            
           
        <div className='row'>

            <div className='form_sub_div col-m-12 col-s-12 col-xs-12'>  
              <div className='flex_start col-m-6 col-xs-12 col-s-12'>
              <div className='col-m-4 col-s-4 col-xs-4'> 

                 <label className='label_fields'> اسم الموظف</label></div>

                 <div className='col-m-8 col-s-8 col-xs-8'>
                 <input
                 className={this.state.empNamClassName}
                 name="empName"
                 value={this.state.data.empName}
                 onChange={this.handlerEmpName}            
                 tabIndex="0"
                 ></input>
                  <InlineError>{this.state.errors.empName}</InlineError>  
                 </div>
                  
                 </div>


                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    <div className='col-m-4 col-s-4 col-xs-4'> 
                    <label className='label_fields'>رقم الوظيفي </label></div>
                    <div className='col-m-8 col-s-8 col-xs-8'>
                   <input 
                    className={this.state.empNumberClassName}
                    name="empNumber"
                    value={this.state.data.empNumber}
                    onChange={this.handlerempNumber}
                    onKeyDown={this.handleKeyDown}
                   ></input>
                    <InlineError>{this.state.errors.empNumber}</InlineError>  
                    </div>
                </div>  
                      
              </div>
        </div>    
         
        <div className='row'>

            <div className='form_sub_div col-m-12 col-s-12 col-xs-12'> 

             <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                  <div className='col-m-4 col-s-4 col-xs-4'>
                   <label className='label_fields'>  كلمة المرور</label></div>
                  <div className='col-m-8 col-s-8 col-xs-8'>
                    <input 
                    type='password'
                     className={this.state.empPasswordClassName}
                     name="empPassword"
                     value={this.state.data.empPassword}
                     onChange={this.handlerEmpPassword}
                    ></input>
                    <InlineError>{this.state.errors.empPassword}</InlineError>  
                    </div>

                </div>        

                <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                    <div className='col-m-4 col-s-4 col-xs-4'>
                        <label className='label_fields'> تأكيد كلمة المرور </label></div>
                    <div className='col-m-8 col-s-8 col-xs-8'>
                        <input
                         type='password'
                         className='input_fields'
                         className={this.state.empRepPasswordClassName}
                         value={this.state.data.empRepPassword}
                         onChange={this.hnadlerRepPassowrd}
                         ></input>
                           <InlineError>{this.state.errors.empRepPassword}</InlineError>  
                         </div>
                </div>

            </div>    
        </div>


        <div className='row'>

          <div className='form_sub_div col-m-12 col-s-12 col-xs-12'> 
              <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                   <div className='col-m-4 col-s-4 col-xs-4'>
                         <label className='label_fields'>الدائرة </label>
                     </div>

                  <div className='col-m-4 col-s-8 col-xs-8'>
                      <select 
                       className={this.state.empDepartmentClassName}
                       onChange={this.handlerDepartment}
                        value={this.state.data.department}
                       >
                      <option value="" disabled selected>قم باختيار الدائرة</option>
                          <option value="zuwar">zuwar</option>
                          <option value="orcas">orcas</option>
                          <option value="BIM">BIM</option>
                          <option value="future">Future</option>
                    </select>
                        <InlineError>{this.state.errors.empDepartment}</InlineError>
                </div>
               

             </div>  


          

            </div>
        </div>

        <div className='row'>

            <div className='button_div col-m-12 col-s-12 col-xs-12 '> 
                {this.state.spinner ?<button className='button_style'>{this.props.userEditType?"تعديل":"ارسال"} </button> :
                <Loader top='auto' left='auto'></Loader>}
            </div>        
            

        </div>        
               
         </form>
        
        </div>

        </div>
        
          
        );
      
    }
}
const mapDispatch=dispatch=>{
    return{
        getId:(id)=>dispatch(getIdUser(id)),
        EditUser:(EditUserData)=>dispatch(EditUser(EditUserData))
    }
}
const mapStateToProps=state=>{
    return{
        userFoundResult:state.pass.userFound
    }
}
export default connect(mapStateToProps,mapDispatch)(createNewUser) 