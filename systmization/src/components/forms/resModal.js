
import './FormRes.css';
import React,{Component} from 'react';
import InlineError from '../messages/inlinError'
import FormEva_edit from './formEva_edit'
import FormRes from './formRes';
import {connect} from 'react-redux';
import {UpdateRes_row} from '../actions/auth'



class reservationRow_edit extends Component{
   state={
       data:{},
       isUpdate:true,
       display: true,
   }
    componentDidMount(){
        this.setState({data:this.props.data})
    }
    handleDisplay(x){
        this.props.deactive()
    }

    UpdateResData=(data)=>{        
        this.setState({data:data})
        this.props.deactive(data)
        
        this.props.updateDateRes(data)
       // console.log(data)
       
    }
    render(){
        return( 
       

        <div className='popup'>
        <button  className="closeButton" onClick={()=>this.handleDisplay("welcome")}>x</button>
        <div className='popup-inner_res'>
            <FormRes data={this.props.data} isUpdate={this.state.isUpdate} updatedData={this.UpdateResData} ></FormRes>
        </div>
        </div>
    
        )
}
}

const mapdispatch = dispatch =>{
    return {
        updateDateRes:(data)=>dispatch(UpdateRes_row(data)),
       
    }
}
const maptToProps=(state)=>{
    return{
    }
  }

export default connect(maptToProps,mapdispatch) (reservationRow_edit)