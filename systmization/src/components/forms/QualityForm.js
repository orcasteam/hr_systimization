import './FormRes.css';
import React,{Component} from 'react';
import {connect} from 'react-redux';
import {insert_Quality,allQualityInsert,updateQuality} from "../actions/auth";
import {NotificationContainer, NotificationManager} from 'react-notifications';
import { stat } from 'fs';
import zuwar from '../images/zuwar.jpg'
var Loader = require('react-loader');   


const today=new Date();
class QualityForm extends Component{
    state={
        quality:'',
        qualityClassName:'textarea_quality',
        qualityerror:'',
        date:today.getFullYear()+'-'+ (today.getMonth()+1)+'-'+today.getDate(),
        spinner:true,
        not:''

    }
    handlerQualityChange=(e)=>{
        this.setState({quality:e.target.value})
        console.log(this.state.quality)

        if(this.state.quality!=="")
    {
        this.setState({qualityClassName:'textarea_quality'});
        this.setState({qualityerror:''})
    }
}
  

    handleSubmit=(e)=>{
        e.preventDefault();
        if(this.props.isUpdate === true){
            
            var obj={note:this.state.quality ,id:this.props.allQuality[0].id};
            this.props.updateQuality(obj);
        }
        else
        if(this.state.quality ==""){
            this.setState({qualityerror:true});
            this.setState({qualityClassName:'textarea_quality_error'})
        }
 
        else{

            var obj={name:this.props.username , date:this.state.date, quality:this.state.quality}
            this.props.qualityInsert(obj)
            this.setState({spinner:false})

            setTimeout(function(){
                this.setState({spinner:true});
                alert(this.props.result)
                if(this.props.result == false)
                {
                    this.setState({not:false})
                }
                else{this.setState({not:true})
            }

            }.bind(this),500)
        }
        this.setState({not:''})
    }


   componentDidMount(){
       if(this.props.isUpdate === true){
           var obj={name:this.props.username , date:this.state.date}
            this.props.allQualityInsert(obj);

            setTimeout(function(){
                
                if(this.props.allQuality){
                this.setState({quality:this.props.allQuality[0].note})}
            }.bind(this),500)

       }
   }
  
    render(){
        return(
            <div className='main_div col-m-12 col-s-12 col-xs-12' >
                    {((this.props.isUpdate && this.props.allQuality) || this.props.isNew) &&

                    <form className='form_div col-m-6 col-s-12 col-s-12'  onSubmit={this.handleSubmit} >
                    <div className='title_div col-m-12 col-s-12 col-xs-12 '> <img src={zuwar} style={{width:120, height:110}}/> </div>   
                    <div className='title_div col-m-12 col-s-12 col-xs-12 '> <h2>تقرير الجودة</h2> </div>
                    
                    <div className='form_sub_div col-m-12 col-s-12 col-xs-12'>  

                    <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                        <div className='col-m-4 col-s-4 col-xs-4'> 
                            <label className='label_fields'> المسؤول</label></div>
                        <div className='col-m-8 col-s-8 col-xs-8'>
                    
                        <label  className='input_fields'>{this.props.username}</label>            
                            </div>
                            
                    </div>

                    <div className='flex_start col-m-6 col-xs-12 col-s-12'>
                        <div className='col-m-4 col-s-4 col-xs-4'> 
                            <label className='label_fields'>التاريخ</label></div>
                        <div className='col-m-8 col-s-8 col-xs-8'>
                        <label  className='input_fields'>{this.state.date}</label>            

                            
                            </div>

                        
                    </div> 

                    </div>

                   
                    <div className='form_sub_div_quality col-m-12 col-s-12 col-xs-12'> 
                    <label className="label_quality"><h4> ملاحظات عامة عن المطعم من إدارة المطعم</h4></label>

                    <textarea cols="50" rows="10" className={this.state.qualityClassName}
                    name='quality'
                    value={this.state.quality}
                    onChange={this.handlerQualityChange}
                    onInput={this.handlerQualityChange}
                    ></textarea>
                    </div>

                    <div className='button_div_qualiy col-m-12 col-s-12 col-xs-12  '> 
                    

                          {this.state.spinner ?<button className='button_style'>حفظ التقرير</button>:
                        <Loader top='auto' left='auto'></Loader>
                }
                    </div> 

                    {this.state.not=== true &&  NotificationManager.success( 'تم اضافة تقرير الجودة بنجاح','تقرير الجودة' )}
                    {this.state.not=== false &&  NotificationManager.error( 'تم اضافة تقرير الجودة لهذا اليوم من قبل' )}

            <NotificationContainer/>
                    </form>
                    }
                    {(this.props.isNew !==true && !this.props.allQuality) && 
                      <div className='main_div col-m-12 col-s-12 col-xs-12' >

                      <form className='form_div col-m-9 col-s-12 col-s-12'   >
                      <div className='title_div col-m-12 col-s-12 col-xs-12 '> <h3> لم يتم اضافة تقييم الجودة من قبل هذا المستخدم </h3> </div> 
                      </form>
                                  
                      </div>
                    
                    }
                                
                    </div>
        )
    }

}

const mapdispatch = dispatch =>{
    return {
       qualityInsert:(data)=>dispatch(insert_Quality(data)),
       allQualityInsert:(data)=>dispatch(allQualityInsert(data)),
       updateQuality:(data)=>dispatch(updateQuality(data)),
    }
}
const maptToProps=(state)=>{
    return{
       username:state.auth.username,
       result:state.pass.allData,
       allQuality:state.pass.allData,
    }
  }

export default  connect(maptToProps,mapdispatch) (QualityForm)