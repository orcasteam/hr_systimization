

import {USER_LOGGED_IN,USER_LOGGED_OUT,USER_TYPE,REVINSERT,getUserId,createdUser,
    displayAllUser,deleteUserG,EVA_EDIT,USER_DELETE,UPDATE_DATE,AllRevData,EVAINSERT,DELETE_RES,UPDATE_RES,INSERT_QUALITY,All_QUALITYDATA,ORCAS_DAILY,ORCAS_Res,ORCAS_Quality,deleteDailyRep,
     deleteREsRep,deleteQualityRep,UPDATEQUALITY} from "./types"
   
import api from "./api"
import Axios from "axios";

export const userLoggedIn = user => {
   return{
    type: USER_LOGGED_IN,
    user
   }
  }
  export const dataRev=(pass)=>{
      return{
          type:REVINSERT,
          pass
      }
  }
  export const getUser=(userFound)=>{
      return{
          type:getUserId,
          userFound
      }
  }
export const creatUser=(userInsert)=>{
    return{
        type:createdUser,
        userInsert

    }

}
export const Alluser=(displayAlluserResult)=>{
    return{
        type:displayAllUser,
        displayAlluserResult

    }
}

  export const dataEva=(evaluation)=>{
    return{
        type:EVAINSERT,
        evaluation
    }
  }
  export const dataEva_edit=(evaluation_edit)=>{
    return{
        type:EVA_EDIT,
        evaluation_edit
    }
  }
  export const dataDelete_row=(evaluation_delete)=>{
      return{
          type:USER_DELETE,
          evaluation_delete
      }

  }
  export const dataUpdate_All=(evaluation_update)=>{
    return{
        type:UPDATE_DATE,
        evaluation_update
    }
  }
  export const getallResData=(allDataResult)=>{
    return{
        type:AllRevData,
        allDataResult
    }
  }
  export const deleteResData=(data)=>{
    return{
        type:DELETE_RES,
        data
    }
}
export const UpdateResData=(data)=>{
    return{
        type:UPDATE_RES,
        data
    }
}

export const QualityInserData=(allDataResult)=>{
    return{
        type:INSERT_QUALITY,
        allDataResult
    }
}

export const allQualityData=(allDataResult)=>{
    return{
        type:All_QUALITYDATA,
        allDataResult
    }
}
export const QualityUpdate=(allDataResult)=>{
    return{
        type:UPDATEQUALITY,
        allDataResult
   }
}


export const login =credentials=>dispatch=>
api.user.login(credentials).then(user=>{
    localStorage.systimization=user.token;
    localStorage.userType=user.types;
    localStorage.username=user.username;

    dispatch(userLoggedIn(user))
    
} )

export const logout=()=>dispatch=>{
    localStorage.removeItem("systimization");
    dispatch(userLogOUt())   
}

export const userLogOUt=()=>{
    return{
        type:USER_LOGGED_OUT
    }
}

export const deleteUserRes=(delteResultres)=>{
    return{
        type:deleteUserG,
        delteResultres

 
    }
}
export const orcasDaily=(reuslt)=>{
  return{
      type:ORCAS_DAILY,
      reuslt

  }
}
export const orcasRes=(reuslt)=>{
    return{
        type:ORCAS_Res,
        reuslt
  
    }
  }
  export const orcasQuality=(reuslt)=>{
    return{
        type:ORCAS_Quality,
        reuslt
  
    }
  }
  export const deleteDaliyRe=(result)=>{
      return{
          type:deleteDailyRep,
          result

      }
  }
  export const deleteResRe=(result)=>{
    return{
        type:deleteREsRep,
        result

    }
}
export const deleteQualityRe=(result)=>{
    return{
        type:deleteQualityRep,
        result

    }
}


export const insertRev=data=>dispatch=>{
    api.insertRev.rev(data).then(pass=>dispatch(dataRev(pass)))
}

export const createNewUser=data=>dispatch=>{
   
    api.createNewUser.createUser(data).then(userInsert=>dispatch(creatUser(userInsert)))
}
export const getIdUser=idUser=>dispatch=>{

    api.getUserId.getUserId(idUser).then(userFound=>dispatch(getUser(userFound)))
}
export const editAllUser=()=>dispatch=>{
   
    api.displayAlluser.displayAlluser().then(displayAlluserResult=>dispatch(Alluser(displayAlluserResult)))
}

export const deleteUser=deleteUserId=>dispatch=>{
    
    api.deleteUser.deleteUser(deleteUserId).then(delteResultres=>dispatch(deleteUserRes(delteResultres)))

}

export const EditUser=EditUserData=>dispatch=>{
    api.EditUser.EditUSer(EditUserData).then()
}


export const insertEva=data=>dispatch=>{
    api.insertEva.eva(data).then(evaluation=>dispatch(dataEva(evaluation)))
    
}
export const insertEva_Edit=data=>dispatch=>{
    api.eva_edit.eva_edit1(data).then(evaluation_edit=>dispatch(dataEva_edit(evaluation_edit)))
   
}

export const delete_row=data=>dispatch=>{
    api.delete_row.delete_row(data).then(evaluation_delete=>dispatch(dataDelete_row(evaluation_delete)))
}

export const update_data=data=>dispatch=>{
    api.update_data.update_data(data).then(evaluation_update=>dispatch(dataUpdate_All(evaluation_update)))
}

export const getAllRes_data=data=>dispatch=>{
    api.getAllRes.getAllRes(data).then(allDataResult=>dispatch(getallResData(allDataResult)))   
}
export const deleteRes=data=>dispatch=>{
    api.delete_res.deleteRes(data).then((deleteresResult=>dispatch(deleteResData(deleteresResult))))
}
export const UpdateRes_row=data=>dispatch=>{
    api.update_res.update_res(data).then(resUpdate=>dispatch(UpdateResData(resUpdate)))
}
export const insert_Quality=data=>dispatch=>{
    api.insert_Quality.insert_Quality(data).then(allDataResult=>dispatch(getallResData(allDataResult)))   
}
export const allQualityInsert=data=>dispatch=>{
    api.getInsert_quality.getInsert_quality(data).then(allDataResult=>dispatch(allQualityData(allDataResult)))
}
export const get_dailyReportOrcas=data=>dispatch=>{
    api.getOrcasDailyReport.OrcasDailyReport(data).then(reuslt=>dispatch(orcasDaily(reuslt)))
}
export const get_ResReportOrcas=data=>dispatch=>{
    api.getZuwerResReports.ZuwerResReports(data).then(reuslt=>dispatch(orcasRes(reuslt)))
}

export const get_QualityReportOrcas=(data)=>dispatch=>{
    api.getZuwerQulityReports.ZuwerQulityReports(data).then(reuslt=>dispatch(orcasQuality(reuslt)))
}
export const deleteDailyReport=(id)=>dispatch=>{
    
    api.deleteDailyReport.deleteDailyReport(id).then(result=>dispatch(deleteDaliyRe(result)))
}
export const deleteResReport=(id)=>dispatch=>{
    api.deleteResReport.deleteResReport(id).then(result=>dispatch(deleteResRe(result)))
}
export const deleteQualityReport=(id)=>dispatch=>{
    api.deleteQualityReport.deleteQualityReport(id).then(result=>dispatch(deleteQualityRe(result)))
}
export const updateQuality=data=>dispatch=>{
    api.updateQualityReport.updateQualityReport(data).then(result=>dispatch(QualityUpdate(result)))
}