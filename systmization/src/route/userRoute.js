import React from "react"
import {Route,Redirect} from "react-router-dom"
import {connect} from "react-redux"

const UserRoute = ({isAuthenticated,component: Component, ...rest }) => (
    <Route
      {...rest}
      render={props =>isAuthenticated? <Component {...props} />:<Redirect to="/"/>}
    />
  );



const mapToProps=(state)=>{
    return{
      isAuthenticated:!!state.auth.token
    }
  }
  export default connect(mapToProps)(UserRoute)