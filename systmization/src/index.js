import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter,Router,Route} from 'react-router-dom'
import {createStore,applyMiddleware} from 'redux'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'
import {composeWithDevTools} from 'redux-devtools-extension'
import rootReducer from './components/reducer/rootReducer'
import { userLoggedIn } from './components/actions/auth';
import 'react-notifications/lib/notifications.css';

const store =createStore(rootReducer,composeWithDevTools(applyMiddleware(thunk)))

if(localStorage.systimization){
    const user={token:localStorage.systimization,types:localStorage.userType,username:localStorage.username}
    store.dispatch(userLoggedIn(user))
}


ReactDOM.render(
<BrowserRouter>
<Provider store={store}>
<Route component={App} />
</Provider>
</BrowserRouter> 
    , document.getElementById('root'));

registerServiceWorker();
