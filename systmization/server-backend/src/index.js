import express from 'express'
import path from 'path'
import bodyParser from 'body-parser';
import jwt from 'jsonwebtoken'
import { callbackify } from 'util';
import { exists } from 'fs';
import { types } from 'util';
require('dotenv').config()


const mysql=require('mysql')

const app =express();

console.log(process.env.host)

const connection=mysql.createConnection({
    host:process.env.host,
    user:process.env.user,
    password:process.env.password ,
    database:process.env.database   
})
function toAuthtication(id){
  return jwt.sign({
      id:id,
  },process.env.securetkey)
}

app.use(bodyParser.json());

app.post("/api/auth",(req,res)=>{
console.log("welcomw")
    connection.connect();
    const {credentials}=req.body;    
    connection.query(`select * from userinfo where username=('${credentials.id}') and password =('${credentials.password}')`,
    function(errors,result,fields){
      
        if(result.length ===1) {
            res.json({user:{token:toAuthtication(credentials.id),types:result[0].type ,username:result[0].username}})

        }else{
            res.status(400).json({result:{global:"Invaild credentail"}})
        }
        connection.end();
    }
    )
})

app.post("/api/rev",(req,res)=>{
    connection.connect();
    const {data}=req.body;
    console.log(req.body)
    const INSERT_NOTE_QUERY=(`INSERT INTO res_tabel ( date, day, name,phone, resp_res, gar_res, price, res_normal, date_res) 
    VALUES ( '${data.date}', '${data.day}', '${data.name}', '${data.phone}', '${data.respons}', '${data.garsone}', '${data.price}', '${data.normalRes}', '${data.res_date}');`);

    connection.query(INSERT_NOTE_QUERY,(errors,results)=>{
     if(!errors){ 
            res.json({revResult:{insertResult:true}});
        }
        });

        console.log(data);
        connection.end();
})


app.post("/api/eva",(req,res)=>{
    connection.connect();
    const {data} =req.body;  
    let x='';
    const testForm_query=(`SELECT * FROM evaluation_table where date=('${data.data.date}') and type=('${data.data.formType}') and shift=('${data.data.shift}') `)
    connection.query(testForm_query,(errors,result,feilds)=>{
        console.log(result.length)
        if(result.length == 0){
            const INSERT_NOTE_QUERY=(`INSERT INTO evaluation_table ( shift_resp, shift, date,day, emergency_state, notes,acheivement, type) 
            VALUES ( '${data.data.shiftResp}', '${data.data.shift}', '${data.data.date}', '${data.data.day}', '${data.data.emergencyState}', '${data.data.notes}', '${data.data.achievement}', '${data.data.formType}');`);
            connection.query(INSERT_NOTE_QUERY,(errors,results)=>{
                if(!errors){ 
                    res.json({evaResult:{insertResult:true}});
        
                    x=results.insertId;
                    console.log(x);
                    console.log(data);
            
                    data.employeestate.map((employee,index)=>{
            
                        
                        const INSERT=(`INSERT INTO employee_status ( id_table, type_em, name,reason) 
                        VALUES ( '${x}', 'absence', '${employee.name}',  '${employee.reason}');`);
                        connection.query(INSERT,(err,res)=>{
                            console.log(x)
                            console.log(res.insertId) 
                        });
                    
                    })
            
                    data.employeeLate.map((employee,index)=>{
            
                    
                        const INSERT=(`INSERT INTO employee_status ( id_table, type_em, name,reason) 
                        VALUES ( '${x}', 'late', '${employee.name}',  '${employee.reason}');`);
                        connection.query(INSERT,(err,res)=>{
                            console.log(x)
                            console.log(res.insertId) 
                        });
                    
                    })
            
                    data.employeeLeft.map((employee,index)=>{
                        
                        const INSERT=(`INSERT INTO employee_status ( id_table, type_em, name,reason) 
                        VALUES ( '${x}', 'left', '${employee.name}',  '${employee.reason}');`);
                        connection.query(INSERT,(err,res)=>{
                            console.log(x)
                            console.log(res.insertId) 
                        });
                 
                    })
            
                }
                else{
                    res.json({evaResult:{insertResult:false}});
                }
                
            //connection.end();
            })
        }
        else{
            res.json({evaResult:{insertResult:false}});

        }
    
    })

});

app.post('/api/eva_edit',(req,res)=>{
    connection.connect();
    //console.log("welcomw")
    console.log(req.body);
    var formType=req.body.data.formType;
    var shift=req.body.data.shift;
    var username=req.body.data.username;
    var today=new Date();
    var date=today.getFullYear()+'-'+ (today.getMonth()+1)+'-'+today.getDate();

    const SELECT_QUERY=(`SELECT * FROM employee_status INNER JOIN evaluation_table on evaluation_table.id=employee_status.id_table where date=('${date}') and type=('${formType}') and shift=('${shift}')`);
    connection.query(SELECT_QUERY,(error,main_table)=>{
       
            res.json({eva_editResult:{eva_editData:main_table}})
           
    })

    connection.end()
})

app.post('/api/delete_row',(req,res)=>{
    connection.connect();
    var id=(req.body.data)
    if(id != null){
        const DELETE_QUERY=`DELETE FROM employee_status WHERE id_em='${id}'`
        connection.query(DELETE_QUERY);

    }
})
app.post('/api/update_data',(req,res)=>{
    connection.connect();
    console.log(req.body.data.data.id);
    const data=req.body.data.data;
    const employeAbsence=req.body.data.employeeAbsence;
    const employeeLate=req.body.data.employeeLate;
    const employeeLeft=req.body.data.employeeLeft;

    if(employeAbsence.length >0) {
        employeAbsence.map((employee,id)=>{
            if(employee.id==null){
                const insertEmployee=(`INSERT INTO employee_status ( id_table, type_em, name,reason) 
                VALUES ( '${employee.id_table}', '${employee.type}', '${employee.name}',  '${employee.reason}');`);
                connection.query(insertEmployee)
            }
            if(employee.id!=null){
                const update_row=(`UPDATE employee_status SET name='${employee.name}' , reason='${employee.reason}' where id_em='${employee.id}'`);
                connection.query(update_row);
            }
        })
    }
    if(employeeLate.length >0 ){
        employeeLate.map((employee,id)=>{
            if(employee.id==null){
                const insertEmployee=(`INSERT INTO employee_status ( id_table, type_em, name,reason) 
                VALUES ( '${employee.id_table}', '${employee.type}', '${employee.name}',  '${employee.reason}');`);
                connection.query(insertEmployee)
            }
            if(employee.id!=null){
                const update_row=(`UPDATE employee_status SET name='${employee.name}' , reason='${employee.reason}' where id_em='${employee.id}'`);
                connection.query(update_row);
            }
        })
    }
    if(employeeLeft.length >0 ){
        employeeLeft.map((employee,id)=>{
            if(employee.id==null){
                const insertEmployee=(`INSERT INTO employee_status ( id_table, type_em, name,reason) 
                VALUES ( '${employee.id_table}', '${employee.type}', '${employee.name}',  '${employee.reason}');`);
                connection.query(insertEmployee)
            }
            if(employee.id!=null){
                const update_row=(`UPDATE employee_status SET name='${employee.name}' , reason='${employee.reason}' where id_em='${employee.id}'`);
                connection.query(update_row);
            }
        })
    }



   const updatequery=`UPDATE evaluation_table SET shift='${data.shift}' ,emergency_state='${data.emergency_state}' ,notes='${data.notes}'
   ,acheivement='${data.acheivement}' where id='${data.id}' `
   connection.query(updatequery);


});
    

app.post('/api/get_allres',(req,res)=>{
    connection.connect();
    //console.log(req.body.data)
    const SELECT_RES_QUERY=`SELECT * FROM res_tabel WHERE resp_res='${req.body.data}'`
    connection.query(SELECT_RES_QUERY,(err,result)=>{
        if(!err){
       // console.log(result)
        res.json({allDataResult:{allData:result}})}
        else{
            console.log(err)
        }
        
    })
})
app.post('/api/delete_res',(req,res)=>{
    connection.connect();
    const DELETE_QUERY=`DELETE FROM res_tabel WHERE id='${req.body.data}'`
    connection.query(DELETE_QUERY);
    console.log(req.body.data)
})

app.post('/api/update_res',(req,res)=>{
    connection.connect();
    var data=req.body.data
    
   const updatequery=`UPDATE res_tabel SET name='${data.name}' ,phone='${data.phone}' ,resp_res='${data.respons}'
   ,gar_res='${data.garsone}', price='${data.price}', res_normal='${data.normalRes}',date_res='${data.res_date}' where id='${data.id}' `
  
   connection.query(updatequery)
  //  connection.end()


})

app.get('/api/displayAllUser',(req,res)=>{
    //console.log("welcometoalluser")
    connection.connect()
    const getAllusers=(`SELECT * FROM users WHERE types !='admin'`)
    connection.query(getAllusers,(errors,results)=>{
        if(errors){
            res.status(400);
            res.end("error:" + errors);
           }
           res.json({displayUser:{displayUser:results}})
          // console.log(results)
        
    })
    //connection.end()
})

app.post('/api/insert_quality',(req,res)=>{
    connection.connect();
    var data=req.body.data;
    const SelectQuality=(`SELECT * FROM quality_table WHERE date=('${data.date}') and name=('${data.name}') `)
    connection.query(SelectQuality,(err,result)=>{
        if(result.length == 0){
            const insertQuality=(`INSERT INTO quality_table ( name, date, note) 
            VALUES ( '${data.name}', '${data.date}', '${data.quality}');`);
            connection.query(insertQuality)
            res.json({allDataResult:{allData:true}})
        }
        else{
            res.json({allDataResult:{allData:false}})
        }
    })
})

app.post('/api/getInsert_quality',(req,res)=>{
    connection.connect();
    const data=req.body.data;

    const SELECT_QUERY=`SELECT * FROM quality_table WHERE date=('${data.date}') and name=('${data.name}')`
    connection.query(SELECT_QUERY,(req,result)=>{
        console.log(result)
        if(result.length > 0){
            res.json({allDataResult:{allData:result}})
        }
       
    })

    console.log(req.body.data)

    connection.end()    
}); 



app.post("/api/editUser",(req,res)=>{
    connection.connect()
    const {EditUserData}=req.body
    console.log(EditUserData)
   
      
  const EditUserQuery=`UPDATE users SET id='${EditUserData.idUserEdit}',
  userName='${EditUserData.empName}',userId='${EditUserData.empNumber}'
  ,password='${EditUserData.empPassword}',types='${EditUserData.department}'
   WHERE id='${EditUserData.idUserEdit}'`

  connection.query(EditUserQuery)


connection.end()
})

app.get('/*',(req,res)=>{

res.sendFile(path.join(__dirname,'index.html'));
})

app.listen(8000,()=>console.log('running on localhost :8000'))


